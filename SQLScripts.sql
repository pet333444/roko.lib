USE [master]
GO
/****** Object:  Database [ROKO.Lib.DB]    Script Date: 21-May-21 18:22:58 ******/
CREATE DATABASE [ROKO.Lib.DB]
 CONTAINMENT = NONE
 
USE [ROKO.Lib.DB]
GO
/****** Object:  UserDefinedTableType [dbo].[dtAuthorsList]    Script Date: 21-May-21 18:22:58 ******/
CREATE TYPE [dbo].[dtAuthorsList] AS TABLE(
	[AuthorId] [int] NULL
)
GO
/****** Object:  Table [dbo].[Authors]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Authors](
	[AuthorId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](200) NULL,
 CONSTRAINT [PK_Authors] PRIMARY KEY CLUSTERED 
(
	[AuthorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AuthorsToLibraryObjects]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthorsToLibraryObjects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AuthorId] [int] NOT NULL,
	[LibraryObjectId] [int] NOT NULL,
 CONSTRAINT [PK_AuthorsToLibraryObjects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Books]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Books](
	[BookId] [int] NOT NULL,
	[PlaceOfPublication] [nvarchar](200) NOT NULL,
	[PublishingHouse] [nvarchar](300) NOT NULL,
	[ISBN] [nvarchar](18) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LibraryObject]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LibraryObject](
	[LibraryObjectId] [int] IDENTITY(1,1) NOT NULL,
	[LibraryObjectType] [nvarchar](10) NOT NULL,
	[Title] [nvarchar](300) NOT NULL,
	[PublicationOrApplicationYear] [int] NOT NULL,
	[NumberOfPages] [int] NOT NULL,
	[Note] [nvarchar](2000) NULL,
	[IsDeleted] [int] NULL,
 CONSTRAINT [PK_LibraryObject] PRIMARY KEY CLUSTERED 
(
	[LibraryObjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Newspapers]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Newspapers](
	[NewspaperId] [int] NOT NULL,
	[PlaceOfPublication] [nvarchar](200) NOT NULL,
	[PublishingHouse] [nvarchar](300) NOT NULL,
	[IssueNumber] [int] NULL,
	[ReleaseDate] [datetime] NOT NULL,
	[ISSN] [nvarchar](14) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Patents]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patents](
	[PatentId] [int] NOT NULL,
	[Country] [nvarchar](200) NOT NULL,
	[RegistrationNumber] [int] NOT NULL,
	[ApplicationDate] [datetime] NULL,
	[PublicationDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LibraryObject] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AuthorsToLibraryObjects]  WITH CHECK ADD  CONSTRAINT [FK_AuthorsToLibraryObjects_Authors] FOREIGN KEY([AuthorId])
REFERENCES [dbo].[Authors] ([AuthorId])
GO
ALTER TABLE [dbo].[AuthorsToLibraryObjects] CHECK CONSTRAINT [FK_AuthorsToLibraryObjects_Authors]
GO
ALTER TABLE [dbo].[AuthorsToLibraryObjects]  WITH CHECK ADD  CONSTRAINT [FK_AuthorsToLibraryObjects_LibraryObject] FOREIGN KEY([LibraryObjectId])
REFERENCES [dbo].[LibraryObject] ([LibraryObjectId])
GO
ALTER TABLE [dbo].[AuthorsToLibraryObjects] CHECK CONSTRAINT [FK_AuthorsToLibraryObjects_LibraryObject]
GO
ALTER TABLE [dbo].[Books]  WITH CHECK ADD  CONSTRAINT [FK_Books_LibraryObject] FOREIGN KEY([BookId])
REFERENCES [dbo].[LibraryObject] ([LibraryObjectId])
GO
ALTER TABLE [dbo].[Books] CHECK CONSTRAINT [FK_Books_LibraryObject]
GO
ALTER TABLE [dbo].[Newspapers]  WITH CHECK ADD  CONSTRAINT [FK_Newspapers_LibraryObject] FOREIGN KEY([NewspaperId])
REFERENCES [dbo].[LibraryObject] ([LibraryObjectId])
GO
ALTER TABLE [dbo].[Newspapers] CHECK CONSTRAINT [FK_Newspapers_LibraryObject]
GO
ALTER TABLE [dbo].[Patents]  WITH CHECK ADD  CONSTRAINT [FK_Patents_LibraryObject] FOREIGN KEY([PatentId])
REFERENCES [dbo].[LibraryObject] ([LibraryObjectId])
GO
ALTER TABLE [dbo].[Patents] CHECK CONSTRAINT [FK_Patents_LibraryObject]
GO
/****** Object:  StoredProcedure [dbo].[AddAuthor]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[AddAuthor]
	@AuthorId int OUTPUT,
	@FirstName nvarchar(50),
	@LastName nvarchar(200)
AS
BEGIN
BEGIN TRANSACTION;
	INSERT INTO [dbo].[Authors] (FirstName, LastName)
	VALUES (@FirstName, @LastName)
	SET @AuthorId = SCOPE_IDENTITY()
COMMIT;
END
GO
/****** Object:  StoredProcedure [dbo].[AddAuthorsToLibraryObjects]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[AddAuthorsToLibraryObjects]
	@AuthorId int,
	@LibraryObjectId int
AS
BEGIN
BEGIN TRANSACTION;
	INSERT INTO [dbo].[AuthorsToLibraryObjects] (AuthorId, LibraryObjectId)
	VALUES (@AuthorId, @LibraryObjectId)
COMMIT;
END
GO
/****** Object:  StoredProcedure [dbo].[AddBook]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddBook]
	@Id int OUTPUT,
	@LibraryObjectType nvarchar(10),
	@Title nvarchar(300),
	@PlaceOfPublication nvarchar(200),
	@PublishingHouse nvarchar(300),
	@PublicationOrApplicationYear int,
	@NumberOfPages int,
	@Note nvarchar(2000),
	@ISBN nvarchar(18)
AS
BEGIN
BEGIN TRANSACTION;
	INSERT INTO [dbo].[LibraryObject] (LibraryObjectType, Title, PublicationOrApplicationYear, NumberOfPages, Note)
	VALUES (@LibraryObjectType, @Title, @PublicationOrApplicationYear, @NumberOfPages, @Note)
	SET @Id = SCOPE_IDENTITY()
	INSERT INTO [dbo].[Books] (BookId, PlaceOfPublication, PublishingHouse, ISBN)
	VALUES (@Id, @PlaceOfPublication, @PublishingHouse, @ISBN)
COMMIT;
END
GO
/****** Object:  StoredProcedure [dbo].[AddNewspaper]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddNewspaper]
	@Id int OUTPUT,
	@LibraryObjectType nvarchar(10),
	@Title nvarchar(300),
	@PlaceOfPublication nvarchar(200),
	@PublishingHouse nvarchar(300),
	@PublicationOrApplicationYear int,
	@NumberOfPages int,
	@ReleaseDate datetime,
	@Note nvarchar(2000),
	@IssueNumber int,
	@ISSN nvarchar(14)
AS
BEGIN
BEGIN TRANSACTION;
	INSERT INTO [dbo].[LibraryObject] (LibraryObjectType, Title, PublicationOrApplicationYear, NumberOfPages, Note)
	VALUES (@LibraryObjectType, @Title, @PublicationOrApplicationYear, @NumberOfPages, @Note)
	SET @Id = SCOPE_IDENTITY()
	INSERT INTO [dbo].[Newspapers] (NewspaperId, PlaceOfPublication, PublishingHouse, ReleaseDate, IssueNumber, ISSN)
	VALUES (@Id, @PlaceOfPublication, @PublishingHouse, @ReleaseDate, @IssueNumber, @ISSN)
COMMIT;
END
GO
/****** Object:  StoredProcedure [dbo].[AddPatent]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddPatent]
	@Id int OUTPUT,
	@LibraryObjectType nvarchar(10),
	@Title nvarchar(300),
	@Country nvarchar(200),
	@RegistrationNumber int,
	@PublicationOrApplicationYear int,
	@NumberOfPages int,
	@ApplicationDate datetime,
	@PublicationDate datetime,
	@Note nvarchar(2000)
AS
BEGIN
BEGIN TRANSACTION;
	INSERT INTO [dbo].[LibraryObject] (LibraryObjectType, Title, PublicationOrApplicationYear, NumberOfPages, Note)
	VALUES (@LibraryObjectType, @Title, @PublicationOrApplicationYear, @NumberOfPages, @Note)
	SET @Id = SCOPE_IDENTITY()
	INSERT INTO [dbo].[Patents] (PatentId, Country, RegistrationNumber, ApplicationDate, PublicationDate)
	VALUES (@Id, @Country, @RegistrationNumber, @ApplicationDate, @PublicationDate)
COMMIT;
END
GO
/****** Object:  StoredProcedure [dbo].[BookUniqueChecker]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[BookUniqueChecker]
	@Title nvarchar(300),
	@PublicationOrApplicationYear int,
	@Authors dtAuthorsList READONLY,
	@ISBN nvarchar(18)
AS
DECLARE @CountOfRepeatings int
BEGIN
	IF @ISBN IS NOT NULL
		BEGIN
		SET @CountOfRepeatings =
		(SELECT COUNT(B.BookId) 
		FROM Books B
		INNER JOIN LibraryObject L ON L.LibraryObjectId = B.BookId
		WHERE B.ISBN = @ISBN
			  AND L.IsDeleted = 0)
		IF @CountOfRepeatings > 0 RETURN 0
		END
	ELSE IF @ISBN IS NULL
		BEGIN
			DECLARE duplicateCursor INSENSITIVE CURSOR FOR
			SELECT L.LibraryObjectID
			FROM Books B
			INNER JOIN LibraryObject L ON B.BookId = L.LibraryObjectId
			WHERE Title = @Title
				  AND PublicationOrApplicationYear = @PublicationOrApplicationYear
				  AND IsDeleted = 0
				  AND ISBN IS NULL

			DECLARE @duplicateIdBook int

			OPEN duplicateCursor
			FETCH NEXT FROM duplicateCursor INTO @duplicateIdBook

			WHILE @@FETCH_STATUS = 0
			BEGIN
				DECLARE @CountOfNonSharedAuthors int
				SET @CountOfNonSharedAuthors =
				(SELECT COUNT(i.AuthorId)
				FROM
				(
					(SELECT AuthorId
					FROM @Authors
                    UNION
                    SELECT AuthorId
                    FROM AuthorsToLibraryObjects
                    WHERE LibraryObjectId = @duplicateIdBook)
                    EXCEPT
                    (SELECT AuthorId --AS AuthorId 
                    FROM @Authors
                    INTERSECT
                    SELECT AuthorId
                    FROM AuthorsToLibraryObjects
                    WHERE LibraryObjectId = @duplicateIdBook)
                    ) i)

					IF @CountOfNonSharedAuthors = 0
					BEGIN
						RETURN 0
					END
					FETCH NEXT FROM duplicateCursor INTO @duplicateIdBook
			END
			CLOSE duplicateCursor
			DEALLOCATE duplicateCursor
	END
	RETURN 1
END
GO
/****** Object:  StoredProcedure [dbo].[DescSortByPubDate]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DescSortByPubDate]
AS
BEGIN
	SELECT LibraryObjectId, LibraryObjectType, Title, PublicationOrApplicationYear, NumberOfPages, Note
	FROM [ROKO.Lib.DB].[dbo].[LibraryObject]
	WHERE IsDeleted = 0
	ORDER BY PublicationOrApplicationYear DESC
END
GO
/****** Object:  StoredProcedure [dbo].[EditAuthor]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EditAuthor]
	@Id int,
	@FirstName nvarchar(50),
	@LastName nvarchar(200)
AS
BEGIN
BEGIN TRANSACTION;
	UPDATE [dbo].[Authors] 
	SET [FirstName] = @FirstName, [LastName] = @LastName
	WHERE AuthorId = @Id
COMMIT;
END
GO
/****** Object:  StoredProcedure [dbo].[EditBook]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EditBook]
	@Id int,
	@Title nvarchar(300),
	@PlaceOfPublication nvarchar(200),
	@PublishingHouse nvarchar(300),
	@PublicationOrApplicationYear int,
	@NumberOfPages int,
	@Note nvarchar(2000),
	@ISBN nvarchar(18)
AS
BEGIN
BEGIN TRANSACTION;
	UPDATE [dbo].[LibraryObject] 
	SET [Title] = @Title, 
		[PublicationOrApplicationYear] = @PublicationOrApplicationYear, 
		[NumberOfPages] = @NumberOfPages,
		[Note] = @Note
	WHERE LibraryObjectId = @Id
	UPDATE [dbo].[Books] 
	SET [PlaceOfPublication] = @PlaceOfPublication,
		[PublishingHouse] = @PublishingHouse,
		[ISBN] = @ISBN
	WHERE BookId = @Id
COMMIT;
END
GO
/****** Object:  StoredProcedure [dbo].[EditNewspaper]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[EditNewspaper]
	@Id int,
	@Title nvarchar(300),
	@PlaceOfPublication nvarchar(200),
	@PublishingHouse nvarchar(300),
	@PublicationOrApplicationYear int,
	@NumberOfPages int,
	@ReleaseDate datetime,
	@Note nvarchar(2000),
	@IssueNumber int,
	@ISSN nvarchar(14)
AS
BEGIN
BEGIN TRANSACTION;
	UPDATE [dbo].[LibraryObject] 
	SET [Title] = @Title, 
		[PublicationOrApplicationYear] = @PublicationOrApplicationYear, 
		[NumberOfPages] = @NumberOfPages,
		[Note] = @Note
	WHERE LibraryObjectId = @Id
	UPDATE [dbo].[Newspapers] 
	SET [PlaceOfPublication] = @PlaceOfPublication,
		[PublishingHouse] = @PublishingHouse,
		[ReleaseDate] = @ReleaseDate,
		[IssueNumber] = @IssueNumber,
		[ISSN] = @ISSN
	WHERE NewspaperId = @Id
COMMIT;
END
GO
/****** Object:  StoredProcedure [dbo].[EditPatent]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[EditPatent]
	@Id int,
	@Title nvarchar(300),
	@Country nvarchar(200),
	@RegistrationNumber int,
	@PublicationOrApplicationYear int,
	@NumberOfPages int,
	@ApplicationDate datetime,
	@PublicationDate datetime,
	@Note nvarchar(2000)
AS
BEGIN
BEGIN TRANSACTION;
	UPDATE [dbo].[LibraryObject] 
	SET [Title] = @Title, 
		[PublicationOrApplicationYear] = @PublicationOrApplicationYear, 
		[NumberOfPages] = @NumberOfPages,
		[Note] = @Note
	WHERE LibraryObjectId = @Id
	UPDATE [dbo].[Patents] 
	SET [Country] = @Country,
		[RegistrationNumber] = @RegistrationNumber,
		[ApplicationDate] = @ApplicationDate,
		[PublicationDate] = @PublicationDate
	WHERE PatentId = @Id
COMMIT;
END
GO
/****** Object:  StoredProcedure [dbo].[FindAllByPublisherNameTemplateAndGroupByPublisher]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[FindAllByPublisherNameTemplateAndGroupByPublisher]
@name nvarchar
AS
BEGIN
	SELECT BookId, Title, PlaceOfPublication, PublishingHouse, PublicationOrApplicationYear, NumberOfPages, Note, ISBN
	FROM [ROKO.Lib.DB].[dbo].[Books]
	JOIN [ROKO.Lib.DB].[dbo].[LibraryObject] 
	ON [BookId] = [LibraryObjectId]
	WHERE [PublishingHouse] LIKE @name + '%' AND IsDeleted = 0
	ORDER BY [PublishingHouse]
END
GO
/****** Object:  StoredProcedure [dbo].[GetAllAuthors]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetAllAuthors]
	
AS
BEGIN
	SELECT AuthorId, FirstName, LastName
	FROM [Authors]
END
GO
/****** Object:  StoredProcedure [dbo].[GetAllCatalog]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllCatalog]
	
AS
BEGIN
	SELECT LibraryObjectId, LibraryObjectType, Title, PublicationOrApplicationYear, NumberOfPages, Note
	FROM [ROKO.Lib.DB].[dbo].[LibraryObject]
	WHERE IsDeleted = 0
END
GO
/****** Object:  StoredProcedure [dbo].[GetBooks]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetBooks]
AS
BEGIN
	select * from LibraryObject
	JOIN [ROKO.Lib.DB].[dbo].[Books] ON [ROKO.Lib.DB].[dbo].[Books].BookId = [ROKO.Lib.DB].[dbo].[LibraryObject].LibraryObjectId
	Left join AuthorsToLibraryObjects on LibraryObject.LibraryObjectId = AuthorsToLibraryObjects.LibraryObjectId
	left join Authors on AuthorsToLibraryObjects.AuthorId = Authors.AuthorId
	WHERE IsDeleted = 0
	order by [LibraryObject].LibraryObjectId
END


GO
/****** Object:  StoredProcedure [dbo].[GetNewspapers]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetNewspapers]
AS
BEGIN
	select * from LibraryObject
	JOIN [ROKO.Lib.DB].[dbo].Newspapers ON [ROKO.Lib.DB].[dbo].Newspapers.NewspaperId = [ROKO.Lib.DB].[dbo].[LibraryObject].LibraryObjectId
	WHERE IsDeleted = 0
	order by [LibraryObject].LibraryObjectId
END


GO
/****** Object:  StoredProcedure [dbo].[GetPatents]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetPatents]
AS
BEGIN
	select * from LibraryObject
	JOIN [ROKO.Lib.DB].[dbo].Patents ON [ROKO.Lib.DB].[dbo].Patents.PatentId = [ROKO.Lib.DB].[dbo].[LibraryObject].LibraryObjectId
	Left join AuthorsToLibraryObjects on LibraryObject.LibraryObjectId = AuthorsToLibraryObjects.LibraryObjectId
	left join Authors on AuthorsToLibraryObjects.AuthorId = Authors.AuthorId
	WHERE IsDeleted = 0
	order by [LibraryObject].LibraryObjectId
END


GO
/****** Object:  StoredProcedure [dbo].[NewspaperUniqueChecker]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[NewspaperUniqueChecker]
@ISSN nvarchar(14),
@Title nvarchar(300),
@PublishingHouse nvarchar(300),
@ReleaseDate datetime
AS
DECLARE @CountOfRepeatNewspaper INT
BEGIN
IF @ISSN IS NULL 
	BEGIN
		SET @CountOfRepeatNewspaper =
		(SELECT COUNT(N.NewspaperId) 
		FROM Newspapers N
		INNER JOIN LibraryObject L ON L.LibraryObjectId = N.NewspaperId
		WHERE L.Title = @Title 
			  AND N.ReleaseDate = @ReleaseDate 
			  AND N.PublishingHouse = @PublishingHouse 
			  AND N.ISSN IS NULL
			  AND L.IsDeleted = 0)
		IF @CountOfRepeatNewspaper > 0 RETURN 1
	END
ELSE IF @ISSN IS NOT NULL 
	BEGIN
		SET @CountOfRepeatNewspaper =
		(SELECT COUNT(LibraryObject.LibraryObjectId)
		FROM Newspapers n
		INNER JOIN LibraryObject ON n.NewspaperId = LibraryObject.LibraryObjectId
		WHERE n.ISSN = @ISSN 
			 --LibraryObject.Title =  (SELECT Title FROM LibraryObject where LibraryObject.Title != @Title)
			  AND LibraryObject.Title != @Title
			  AND LibraryObject.IsDeleted = 0)
			
		IF @CountOfRepeatNewspaper > 0
		BEGIN
			RETURN 1
		END
	END
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[PatentUniqueChecker]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[PatentUniqueChecker]
	@Country nvarchar(200),
	@RegistrationNumber int
AS
BEGIN
	DECLARE @CountOfIds bit

	SELECT @CountOfIds = COUNT(PatentId) 
	FROM Patents
	WHERE [Country] = @Country AND [RegistrationNumber] = @RegistrationNumber
	
	IF @CountOfIds > 0
	RETURN 0
	ELSE
	RETURN 1
END
GO
/****** Object:  StoredProcedure [dbo].[RemoveLibraryObject]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[RemoveLibraryObject]
@Id int 
AS
BEGIN
	UPDATE [ROKO.Lib.DB].[dbo].[LibraryObject] 
	SET IsDeleted = 1
	WHERE LibraryObjectId = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[SearchBookAndPatentByAuthor]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SearchBookAndPatentByAuthor]
	@FirstName nvarchar(50),
	@LastName nvarchar(200)
AS
BEGIN
	SELECT * FROM [ROKO.Lib.DB].[dbo].[Authors]
	JOIN [ROKO.Lib.DB].[dbo].[AuthorsToLibraryObjects] ON [ROKO.Lib.DB].[dbo].[Authors].[AuthorId] = [ROKO.Lib.DB].[dbo].[AuthorsToLibraryObjects].[AuthorId]
	JOIN [ROKO.Lib.DB].[dbo].[LibraryObject] ON [ROKO.Lib.DB].[dbo].[LibraryObject].[LibraryObjectId] = [ROKO.Lib.DB].[dbo].[AuthorsToLibraryObjects].[LibraryObjectId]
	WHERE [LibraryObjectType] like N'Book' and [LibraryObjectType] like N'Patent'
	AND 
	[FirstName] LIKE '%'+@FirstName+'%' OR [LastName] LIKE '%'+@LastName+'%'
	AND IsDeleted=0
	ORDER BY LibraryObject.LibraryObjectId
END
GO
/****** Object:  StoredProcedure [dbo].[SearchBookByAuthor]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[SearchBookByAuthor]
	@FirstName nvarchar(50),
	@LastName nvarchar(200)
AS
BEGIN
	SELECT * FROM [ROKO.Lib.DB].[dbo].[Authors]
	JOIN [ROKO.Lib.DB].[dbo].[AuthorsToLibraryObjects] ON [ROKO.Lib.DB].[dbo].[Authors].[AuthorId] = [ROKO.Lib.DB].[dbo].[AuthorsToLibraryObjects].[AuthorId]
	JOIN [ROKO.Lib.DB].[dbo].[LibraryObject] ON [ROKO.Lib.DB].[dbo].[LibraryObject].[LibraryObjectId] = [ROKO.Lib.DB].[dbo].[AuthorsToLibraryObjects].[LibraryObjectId]
	JOIN [ROKO.Lib.DB].[dbo].[Books] ON [ROKO.Lib.DB].[dbo].[Books].BookId = [ROKO.Lib.DB].[dbo].[LibraryObject].LibraryObjectId
	WHERE [LibraryObjectType] LIKE N'Book' 
	AND 
	[FirstName] LIKE '%'+@FirstName+'%' OR [LastName] LIKE '%'+@LastName+'%'
	AND IsDeleted=0
END
GO
/****** Object:  StoredProcedure [dbo].[SearchByTitle]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[SearchByTitle]
@title nvarchar
AS
BEGIN
	SELECT LibraryObjectId, LibraryObjectType, Title, PublicationOrApplicationYear, NumberOfPages, Note
	FROM [ROKO.Lib.DB].[dbo].[LibraryObject]
	WHERE [Title] LIKE '%' + @title + '%' 
	AND IsDeleted = 0
END
GO
/****** Object:  StoredProcedure [dbo].[SearchPatentByAuthor]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[SearchPatentByAuthor]
	@FirstName nvarchar(50),
	@LastName nvarchar(200)
AS
BEGIN
	SELECT * FROM [ROKO.Lib.DB].[dbo].[Authors]
	JOIN [ROKO.Lib.DB].[dbo].[AuthorsToLibraryObjects] ON [ROKO.Lib.DB].[dbo].[Authors].[AuthorId] = [ROKO.Lib.DB].[dbo].[AuthorsToLibraryObjects].[AuthorId]
	JOIN [ROKO.Lib.DB].[dbo].[LibraryObject] ON [ROKO.Lib.DB].[dbo].[LibraryObject].[LibraryObjectId] = [ROKO.Lib.DB].[dbo].[AuthorsToLibraryObjects].[LibraryObjectId]
	JOIN [ROKO.Lib.DB].[dbo].[Patents] ON [ROKO.Lib.DB].[dbo].[Patents].PatentId = [ROKO.Lib.DB].[dbo].[LibraryObject].LibraryObjectId
	WHERE [LibraryObjectType] LIKE N'Patent' 
	AND 
	[FirstName] LIKE '%'+@FirstName+'%' OR [LastName] LIKE '%'+@LastName+'%'
	AND IsDeleted=0
END
GO
/****** Object:  StoredProcedure [dbo].[SortByPubDate]    Script Date: 21-May-21 18:22:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SortByPubDate]
AS
BEGIN
	SELECT LibraryObjectId, LibraryObjectType, Title, PublicationOrApplicationYear, NumberOfPages, Note
	FROM [ROKO.Lib.DB].[dbo].[LibraryObject]
	WHERE IsDeleted = 0
	ORDER BY PublicationOrApplicationYear
END
GO
USE [master]
GO
ALTER DATABASE [ROKO.Lib.DB] SET  READ_WRITE 
GO


CREATE PROCEDURE SelectAuthorsByLibraryElementID
	@ID int
AS
BEGIN

	declare @LibraryElementsId table (ID int)

	insert into @LibraryElementsId
		Select AuthorId 
			from AuthorsToLibraryObjects
				where LibraryObjectId = @ID

	Select AuthorId,FirstName,LastName
		from Authors
			where AuthorId in (Select ID 
								from @LibraryElementsId)
END
GO