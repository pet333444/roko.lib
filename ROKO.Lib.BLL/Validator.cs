﻿using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;

using System;
using System.Linq;

namespace ROKO.Lib.BLL
{
    public abstract class Validator<T> : IValidator<T>
    {
        private readonly ILogger<Validator<T>> _logger;

        public const int FirstNameMaxLength = 50;
        public const int LastNameMaxLength = 200;
        public const int PlaceNameMaxLength = 200;
        public const int TitleMaxLength = 300;
        public const int PublisherNameMaxLength = 300;
        public const int NoteMaxLength = 2000;
        public const int MinPublicationYear = 1400;
        public const int MinPatentYear = 1474;
        public const int MaxNumberOfRegNum = 999999999;

        public Validator()
        {
        }

        public Validator(ILogger<Validator<T>> logger)
        {
            _logger = logger;
        }

        public abstract bool Check(T inp);

        public bool IsFirstUpper(string input)
        {
            try
            {
                var startSymbol = input[0];

                return Char.ToUpper(startSymbol) == startSymbol;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.Validator<T>)} Method: {nameof(BLL.Validator<T>.IsFirstUpper)}");
                return false;
            }
        }

        public bool SpaceChecker(string input)
        {
            try
            {
                if (input.StartsWith(" ") || input.EndsWith(" "))
                {
                    return false;
                }

                for (int i = 1; i < input.Length; i++)
                {
                    if ((input[i - 1] == ' ') && ((!char.IsLetter(input[i])) || (!char.IsUpper(input[i]))))
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.Validator<T>)} Method: {nameof(BLL.Validator<T>.SpaceChecker)}");
                return false;
            }
        }

        public bool IsHyphenOrApostropheFirstOrLast(string input)
        {
            try
            {
                if ((input[0] == '-' || input[input.Length - 1] == '-') || (input[0] == '\'' || input[input.Length - 1] == '\''))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.Validator<T>)} Method: {nameof(BLL.Validator<T>.IsHyphenOrApostropheFirstOrLast)}");
                return false;
            }
        }

        public bool IsUpperAfterApostrophe(string input)
        {
            try
            {
                if (!input.Contains('\''))
                {
                    return true;
                }

                var count = input.Count(n => n == '\'');

                if (count == 1)
                {
                    var apostrophePos = input.IndexOf('\'');

                    var nextFromApostrophe = input[apostrophePos + 1];

                    if (char.IsLetter(nextFromApostrophe) && char.IsUpper(nextFromApostrophe))
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.Validator<T>)} Method: {nameof(BLL.Validator<T>.IsUpperAfterApostrophe)}");
                return false;
            }
        }

        public bool IsUpperAfterSingleOrDoubleHyphen(string input)
        {
            try
            {
                if (!input.Contains('-'))
                {
                    return true;
                }

                var count = input.Count(n => n == '-');

                if (count == 1)
                {
                    var hyphenPos = input.IndexOf('-');

                    var letterAfterHyphen = input[hyphenPos + 1];

                    return char.ToUpper(letterAfterHyphen) == letterAfterHyphen;
                }

                if (count == 2)
                {
                    string clone = input;

                    int flag = 0;

                    for (int i = 0; i < 2; i++)
                    {
                        var hyphenPos = clone.IndexOf('-');

                        var nextFromHyphen = clone[hyphenPos + 1];

                        if (i == 0 && char.IsUpper(nextFromHyphen))
                        {
                            return false;
                        }

                        clone = clone.Remove(0, hyphenPos + 1);

                        if (!(char.IsLetter(nextFromHyphen) && char.IsUpper(nextFromHyphen)) && i == 1)
                        {
                            flag++;
                        }
                    }

                    return flag == 0;
                }

                return false;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.Validator<T>)} Method: {nameof(BLL.Validator<T>.IsUpperAfterSingleOrDoubleHyphen)}");
                return false;
            }
        }

        public bool PublishingYearChecker(int pubYear)
        {
            try
            {
                int currentYear = DateTime.Now.Year;

                return !(pubYear > currentYear);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.Validator<T>)} Method: {nameof(BLL.Validator<T>.PublishingYearChecker)}");
                return false;
            }
        }

        public bool IsOnlyOneLanguage(string input)
        {
            string russianAlphabet = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            char[] rusAlpCharArray = russianAlphabet.ToCharArray();
            string latinAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            char[] latinAlpCharArray = latinAlphabet.ToCharArray();

            try
            {
                bool result = false;

                if (input.IndexOfAny(rusAlpCharArray) != -1)
                {
                    if (input.IndexOfAny(latinAlpCharArray) != -1)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                else
                {
                    result = true;
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.Validator<T>)} Method: {nameof(BLL.Validator<T>.IsOnlyOneLanguage)}");
                return false;
            }
        }
    }
}