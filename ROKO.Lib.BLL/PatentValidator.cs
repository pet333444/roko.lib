﻿using Microsoft.Extensions.Logging;

using ROKO.Lib.Entities;

using System;

namespace ROKO.Lib.BLL
{
    public class PatentValidator : Validator<Patent>
    {
        private readonly AuthorValidator _authorValidator;

        private readonly ILogger<Validator<Patent>> _logger;

        public PatentValidator(ILogger<Validator<Patent>> logger) : base(logger)
        {
            _authorValidator = new AuthorValidator();
            _logger = logger;
        }

        public override bool Check(Patent inp)
        {
            int flag = 0;

            if (inp.Title.Length > TitleMaxLength)
            {
                _logger.LogError($"ErrorMessage: Title length error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentValidator)} Method: {nameof(BLL.PatentValidator.Check)}");
                flag++;
            }

            if (!IsOnlyOneLanguage(inp.Country))
            {
                _logger.LogError($"ErrorMessage: Country language error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentValidator)} Method: {nameof(BLL.PatentValidator.Check)}");
                flag++;
            }

            if (!IsFirstUpper(inp.Country))
            {
                _logger.LogError($"ErrorMessage: Country starts with upper error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentValidator)} Method: {nameof(BLL.PatentValidator.Check)}");
                flag++;
            }

            if (inp.Country.Length > PlaceNameMaxLength)
            {
                _logger.LogError($"ErrorMessage: Country length error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentValidator)} Method: {nameof(BLL.PatentValidator.Check)}");
                flag++;
            }

            if (inp.RegistrationNumber < 0)
            {
                _logger.LogError($"ErrorMessage: RegNumber cannot be negative number. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentValidator)} Method: {nameof(BLL.PatentValidator.Check)}");
                flag++;
            }

            if (inp.RegistrationNumber > MaxNumberOfRegNum)
            {
                _logger.LogError($"ErrorMessage: RegNumber cannot be negative and more than nine-digit number. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentValidator)} Method: {nameof(BLL.PatentValidator.Check)}");
                flag++;
            }

            if (inp.ApplicationDate == null)
            {
                if (inp.PublicationDate.Year < MinPatentYear)
                {
                    _logger.LogError($"ErrorMessage: Publication Date < min publication date border error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentValidator)} Method: {nameof(BLL.PatentValidator.Check)}");
                    flag++;
                }

                if (inp.PublicationDate >= DateTime.Now)
                {
                    _logger.LogError($"ErrorMessage: Publication Date >= current date error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentValidator)} Method: {nameof(BLL.PatentValidator.Check)}");
                    flag++;
                }
            }
            else
            {
                if (inp.PublicationDate.Year < MinPatentYear)
                {
                    _logger.LogError($"ErrorMessage: Publication Date < min publication date border error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentValidator)} Method: {nameof(BLL.PatentValidator.Check)}");
                    flag++;
                }

                if (inp.PublicationDate >= DateTime.Now)
                {
                    _logger.LogError($"ErrorMessage: Publication Date >= current date error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentValidator)} Method: {nameof(BLL.PatentValidator.Check)}");
                    flag++;
                }

                if (inp.ApplicationDate.Value.Year < MinPatentYear)
                {
                    _logger.LogError($"ErrorMessage: Application Date < min date border error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentValidator)} Method: {nameof(BLL.PatentValidator.Check)}");
                    flag++;
                }

                if (inp.PublicationDate < inp.ApplicationDate)
                {
                    _logger.LogError($"ErrorMessage: Publication Date < application date error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentValidator)} Method: {nameof(BLL.PatentValidator.Check)}");
                    flag++;
                }
            }

            if (inp.NumberOfPages < 0)
            {
                _logger.LogError($"ErrorMessage: Number of pages error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentValidator)} Method: {nameof(BLL.PatentValidator.Check)}");
                flag++;
            }

            if (inp.Note.Length > NoteMaxLength)
            {
                _logger.LogError($"ErrorMessage: Note length error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentValidator)} Method: {nameof(BLL.PatentValidator.Check)}");
                flag++;
            }

            var authors = inp.Authors;

            if (authors != null)
            {
                for (int i = 0; i < authors.Count; i++)
                {
                    if (!_authorValidator.Check(authors[i]))
                    {
                        _logger.LogError($"ErrorMessage: Authors error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentValidator)} Method: {nameof(BLL.PatentValidator.Check)}");
                        flag++;
                    }
                }
            }

            return flag == 0;
        }
    }
}