﻿using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;

using System;
using System.Collections.Generic;

namespace ROKO.Lib.BLL
{
    public class CatalogLogic : ICatalogLogic
    {
        private readonly ICatalogDAO _catalogDAO;

        private readonly ILogger<CatalogLogic> _logger;

        public CatalogLogic(ICatalogDAO catalogDAO, ILogger<CatalogLogic> logger)
        {
            _catalogDAO = catalogDAO;
            _logger = logger;
        }

        public bool Remove(int id)
        {
            try
            {
                return _catalogDAO.Remove(id);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.CatalogLogic)} Method: {nameof(BLL.CatalogLogic.Remove)}");
                return false;
            }
        }

        public IEnumerable<LibraryObject> Sort(SortingCriterion criterion)
        {
            try
            {
                return _catalogDAO.Sort(criterion);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.CatalogLogic)} Method: {nameof(BLL.CatalogLogic.Sort)}");
                return null;
            }
        }

        public IEnumerable<LibraryObject> FindBooksAndPatentsOfAuthor(Author author)
        {
            try
            {
                return _catalogDAO.FindBooksAndPatentsOfAuthor(author);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.CatalogLogic)} Method: {nameof(BLL.CatalogLogic.FindBooksAndPatentsOfAuthor)}");
                return null;
            }
        }

        public IEnumerable<LibraryObject> GetCatalog()
        {
            try
            {
                return _catalogDAO.GetCatalog();
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.CatalogLogic)} Method: {nameof(BLL.CatalogLogic.GetCatalog)}");
                return null;
            }
        }

        public IEnumerable<LibraryObject> GetRecordByTitle(string title)
        {
            try
            {
                return _catalogDAO.GetRecordByTitle(title);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.CatalogLogic)} Method: {nameof(BLL.CatalogLogic.GetRecordByTitle)}");
                return null;
            }
        }

        public Dictionary<int, List<LibraryObject>> GroupRecordsByYear()
        {
            try
            {
                return _catalogDAO.GroupRecordsByYear();
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.CatalogLogic)} Method: {nameof(BLL.CatalogLogic.GroupRecordsByYear)}");
                return null;
            }
        }

        public IEnumerable<LibraryObject> FindBookByAuthor(Author author)
        {
            try
            {
                return _catalogDAO.FindBookByAuthor(author);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.CatalogLogic)} Method: {nameof(BLL.CatalogLogic.FindBookByAuthor)}");
                return null;
            }
        }

        public IEnumerable<LibraryObject> FindPatentByAuthor(Author author)
        {
            try
            {
                return _catalogDAO.FindPatentByAuthor(author);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.CatalogLogic)} Method: {nameof(BLL.CatalogLogic.FindPatentByAuthor)}");
                return null;
            }
        }

        public LibraryObject FindCatalogElementById(int id)
        {
            try
            {
                return _catalogDAO.FindCatalogElementById(id);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.CatalogLogic)} Method: {nameof(BLL.CatalogLogic.FindCatalogElementById)}");
                return null;
            }
        }
    }
}