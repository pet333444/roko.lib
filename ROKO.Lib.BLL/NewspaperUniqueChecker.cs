﻿using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;

using System;

namespace ROKO.Lib.BLL
{
    public class NewspaperUniqueChecker : IUniqueChecker<Newspaper>
    {
        private readonly ICatalogUnitDAO<Newspaper> _newspaperSQLDao;

        private readonly ILogger<NewspaperUniqueChecker> _logger;

        public NewspaperUniqueChecker(ICatalogUnitDAO<Newspaper> inpdao, ILogger<NewspaperUniqueChecker> logger)
        {
            _newspaperSQLDao = inpdao;
            _logger = logger;
        }

        public bool Check(Newspaper newspaper)
        {
            try
            {
                return _newspaperSQLDao.Check(newspaper);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.NewspaperUniqueChecker)} Method: {nameof(BLL.NewspaperUniqueChecker.Check)}");
                return false;
            }
        }
    }
}