﻿using Microsoft.Extensions.DependencyInjection;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.Entities;

namespace ROKO.Lib.BLL
{
    public static class LogicConfigurator
    {
        public static IServiceCollection AddLogic(this IServiceCollection services)
        {
            return services
                            .AddTransient<ICatalogLogic, CatalogLogic>()
                            .AddTransient<IBookLogic, BookLogic>()
                            .AddTransient<ICatalogUnitLogic<Newspaper>, CatalogUnitLogic<Newspaper>>()
                            .AddTransient<ICatalogUnitLogic<Patent>, CatalogUnitLogic<Patent>>()
                            .AddTransient<IAuthorLogic, AuthorLogic>()

                            .AddTransient<IValidator<Book>, BookValidator>()
                            .AddTransient<IValidator<Newspaper>, NewspaperValidator>()
                            .AddTransient<IValidator<Patent>, PatentValidator>()

                            .AddTransient<IUniqueChecker<Book>, BookUniqueChecker>()
                            .AddTransient<IUniqueChecker<Newspaper>, NewspaperUniqueChecker>()
                            .AddTransient<IUniqueChecker<Patent>, PatentUniqueChecker>()

                            .AddTransient<IUserLogic, UserLogic>();
        }
    }
}