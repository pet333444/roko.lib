﻿using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;

using System;
using System.Collections.Generic;

namespace ROKO.Lib.BLL
{
    public class AuthorLogic : IAuthorLogic
    {
        private readonly IAuthorDAO _authorSQLDao;

        private readonly ILogger<AuthorLogic> _logger;

        public AuthorLogic(IAuthorDAO authorDAO, ILogger<AuthorLogic> logger)
        {
            _authorSQLDao = authorDAO;
            _logger = logger;
        }

        public CatalogUnitWithAuthors AddAuthors(CatalogUnitWithAuthors unit)
        {
            try
            {
                return _authorSQLDao.AddAuthors(unit);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.AuthorLogic)} Method: {nameof(BLL.AuthorLogic.AddAuthors)}");
                return null;
            }
        }

        public void AddAuthorsToLibraryObject(int libraryObjectId, IEnumerable<int> authorsIds)
        {
            try
            {
                _authorSQLDao.AddAuthorsToLibraryObject(libraryObjectId, authorsIds);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.AuthorLogic)} Method: {nameof(BLL.AuthorLogic.AddAuthorsToLibraryObject)}");
            }
        }

        public IEnumerable<Author> GetAuthors()
        {
            try
            {
                return _authorSQLDao.GetAuthors();
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.AuthorLogic)} Method: {nameof(BLL.AuthorLogic.GetAuthors)}");
                return null;
            }
        }

        public IEnumerable<Author> GetAuthorsByLibraryElementID(int ID)
        {
            try
            {
                return _authorSQLDao.GetAuthorsByLibraryElementID(ID);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.AuthorLogic)} Method: {nameof(BLL.AuthorLogic.GetAuthorsByLibraryElementID)}");
                return null;
            }
        }
    }
}