﻿using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;

using System;
using System.Collections.Generic;

namespace ROKO.Lib.BLL
{
    public class CatalogUnitLogic<T> : ICatalogUnitLogic<T> where T : LibraryObject
    {
        protected static ICatalogUnitDAO<T> _catalogUnitDAO;

        private readonly IUniqueChecker<T> _tController;

        private readonly IValidator<T> _tValidator;

        private readonly ILogger<CatalogUnitLogic<T>> _logger;

        public CatalogUnitLogic(ICatalogUnitDAO<T> catalogUnitDAO, IValidator<T> catalogValidator, IUniqueChecker<T> catalogUniqueChecker, ILogger<CatalogUnitLogic<T>> logger)
        {
            _catalogUnitDAO = catalogUnitDAO;
            _tValidator = catalogValidator;
            _tController = catalogUniqueChecker;

            _logger = logger;
        }

        public int Add(T unit)
        {
            try
            {
                if (_tValidator.Check(unit) /*&& _tController.Check(unit)*/)
                {
                    return _catalogUnitDAO.Add(unit);
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.CatalogUnitLogic<T>)} Method: {nameof(BLL.CatalogUnitLogic<T>.Add)}");
                return -1;
            }
        }

        public void Edit(T unit)
        {
            try
            {
                if (_tValidator.Check(unit) /*&& _tController.Check(unit)*/)
                {
                    _catalogUnitDAO.Edit(unit);
                }
                else
                {
                    //TODO: change void to int
                    Console.WriteLine("Validation Exception!!!");
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.CatalogUnitLogic<T>)} Method: {nameof(BLL.CatalogUnitLogic<T>.Edit)}");
            }
        }

        public IEnumerable<T> GetAllSpecified()
        {
            try
            {
                return _catalogUnitDAO.GetAllSpecified();
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.CatalogUnitLogic<T>)} Method: {nameof(BLL.CatalogUnitLogic<T>.GetAllSpecified)}");
                return null;
            }
        }
    }
}