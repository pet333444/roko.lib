﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ROKO.Lib.DAL.SQL
{
    public class BookSQLDao : IBookDAO, IUniqueCheckerDAO<Book>
    {
        private readonly IConfiguration _configuration;

        private readonly ILogger<BookSQLDao> _logger;

        public BookSQLDao(IConfiguration configuration, ILogger<BookSQLDao> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public int Add(Book unit)
        {
            var unitType = typeof(Book).Name;
            int unitId = -1;

            var result = new List<LibraryObject>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("AddBook", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterId = new SqlParameter("@Id", unit.Id);
                    command.Parameters.Add(parameterId);

                    SqlParameter parameterLibObjType = new SqlParameter("@LibraryObjectType", unitType);
                    command.Parameters.Add(parameterLibObjType);

                    SqlParameter parameterTitle = new SqlParameter("@Title", unit.Title);
                    command.Parameters.Add(parameterTitle);

                    SqlParameter parameterPlaceOfPublication = new SqlParameter("@PlaceOfPublication", unit.PlaceOfPublication);
                    command.Parameters.Add(parameterPlaceOfPublication);

                    SqlParameter parameterPublishingHouse = new SqlParameter("@PublishingHouse", unit.PublishingHouse);
                    command.Parameters.Add(parameterPublishingHouse);

                    SqlParameter parameterPubOrAppYear = new SqlParameter("@PublicationOrApplicationYear", unit.PublicationOrApplicationYear);
                    command.Parameters.Add(parameterPubOrAppYear);

                    SqlParameter parameterNumberOfPages = new SqlParameter("@NumberOfPages", unit.NumberOfPages);
                    command.Parameters.Add(parameterNumberOfPages);

                    SqlParameter parameterNote = new SqlParameter("@Note", unit.Note);
                    command.Parameters.Add(parameterNote);

                    SqlParameter parameterISBN = new SqlParameter("@ISBN", unit.ISBN);
                    command.Parameters.Add(parameterISBN);

                    connection.Open();

                    command.Parameters["@Id"].Direction = System.Data.ParameterDirection.Output;

                    var reader = command.ExecuteNonQuery();

                    unitId = (int)command.Parameters["@Id"].Value;

                    unit.Id = unitId;

                    command.Dispose();

                    return unitId;
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.BookSQLDao)} Method: {nameof(DAL.SQL.BookSQLDao.Add)}");
                return -1;
            }
        }

        public bool Check(Book book)
        {
            var dtAuthors = new DataTable("dtAuthorsList");

            dtAuthors.Columns.Add("AuthorId", typeof(int));

            foreach (var item in book.Authors)
            {
                DataRow row = dtAuthors.NewRow();
                row["AuthorId"] = item.AuthorID;
                dtAuthors.Rows.Add(row);
            }

            bool returnValue = false;

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("BookUniqueChecker", connection);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter parameterTitle = new SqlParameter("@Title", book.Title);
                    command.Parameters.Add(parameterTitle);

                    SqlParameter parameterPublicationOrApplicationYear = new SqlParameter("@PublicationOrApplicationYear", book.PublicationOrApplicationYear);
                    command.Parameters.Add(parameterPublicationOrApplicationYear);

                    SqlParameter parameterAuthors = new SqlParameter("@Authors", dtAuthors);
                    command.Parameters.Add(parameterAuthors);

                    SqlParameter parameterISBN = new SqlParameter("@ISBN", book.ISBN);
                    command.Parameters.Add(parameterISBN);

                    SqlParameter parameterReturnValue = new SqlParameter("@return_value", SqlDbType.Bit);
                    command.Parameters.Add(parameterReturnValue);

                    connection.Open();

                    command.Parameters["@return_value"].Direction = ParameterDirection.ReturnValue;

                    var reader = command.ExecuteNonQuery();

                    returnValue = Convert.ToBoolean(command.Parameters["@return_value"].Value);
                }

                if (returnValue == true)
                {
                    //Console.WriteLine("There are no matches. Thats all good, thoe!");
                    return true;
                }
                else
                {
                    //Console.WriteLine("There is a collision, element with this creds is already exists!");
                    return false;
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.BookSQLDao)} Method: {nameof(DAL.SQL.BookSQLDao.Check)}");
                return false;
            }
        }

        public void Edit(Book book)
        {
            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("EditBook", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterId = new SqlParameter("@Id", book.Id);
                    command.Parameters.Add(parameterId);

                    SqlParameter parameterTitle = new SqlParameter("@Title", book.Title);
                    command.Parameters.Add(parameterTitle);

                    SqlParameter parameterPlaceOfPublication = new SqlParameter("@PlaceOfPublication", book.PlaceOfPublication);
                    command.Parameters.Add(parameterPlaceOfPublication);

                    SqlParameter parameterPublishingHouse = new SqlParameter("@PublishingHouse", book.PublishingHouse);
                    command.Parameters.Add(parameterPublishingHouse);

                    SqlParameter parameterPubOrAppYear = new SqlParameter("@PublicationOrApplicationYear", book.PublicationOrApplicationYear);
                    command.Parameters.Add(parameterPubOrAppYear);

                    SqlParameter parameterNumberOfPages = new SqlParameter("@NumberOfPages", book.NumberOfPages);
                    command.Parameters.Add(parameterNumberOfPages);

                    SqlParameter parameterNote = new SqlParameter("@Note", book.Note);
                    command.Parameters.Add(parameterNote);

                    SqlParameter parameterISBN = new SqlParameter("@ISBN", book.ISBN);
                    command.Parameters.Add(parameterISBN);

                    connection.Open();

                    var reader = command.ExecuteNonQuery();

                    command.Dispose();
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.BookSQLDao)} Method: {nameof(DAL.SQL.BookSQLDao.Edit)}");
            }
        }

        public Dictionary<string, List<Book>> FindAllByPublisherNameTemplateAndGroupByPublisher(string publisherNameTemplate)
        {
            var result = new Dictionary<string, List<Book>>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("FindAllByPublisherNameTemplateAndGroupByPublisher", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterPublisherNameTemplate = new SqlParameter("@name", publisherNameTemplate);
                    command.Parameters.Add(parameterPublisherNameTemplate);

                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var temp =
                                new Book(
                                    (string)reader["Title"],
                                    (string)reader["PlaceOfPublication"],
                                    (string)reader["PublishingHouse"],
                                    (int)reader["PublicationOrApplicationYear"],
                                    (int)reader["NumberOfPages"]
                                    );

                            if (result.ContainsKey(temp.PublishingHouse))
                            {
                                result[temp.PublishingHouse].Add(temp);
                            }
                            else
                            {
                                result.Add(temp.PublishingHouse, new List<Book> { temp });
                            }
                        }
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.BookSQLDao)} Method: {nameof(DAL.SQL.BookSQLDao.FindAllByPublisherNameTemplateAndGroupByPublisher)}");
                return null;
            }
        }

        public IEnumerable<Book> GetAllSpecified()
        {
            var result = new List<Book>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("GetBooks", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        var currentId = -1;
                        var indexOfLast = -1;
                        while (reader.Read())
                        {
                            if (currentId == (int)reader["LibraryObjectId"])
                            {
                                var authorFirstName = (string)reader["FirstName"];
                                var authorLastName = (string)reader["LastName"];

                                if (authorFirstName != null && authorLastName != null)
                                {
                                    Author author = new Author(authorFirstName, authorLastName);
                                    author.AuthorID = (int)reader["AuthorId"];
                                    result[indexOfLast].Authors.Add(author);
                                }
                            }
                            else
                            {
                                Book book = new Book(
                                (string)reader["Title"],
                                (string)reader["PlaceOfPublication"],
                                (string)reader["PublishingHouse"],
                                (int)reader["PublicationOrApplicationYear"],
                                (int)reader["NumberOfPages"],
                                new List<Author>(),
                                (string)reader["Note"],
                                (string)reader["ISBN"]
                                );
                                book.Id = (int)reader["LibraryObjectId"];
                                book.Type = (string)reader["LibraryObjectType"];

                                var authorFirstName = Convert.IsDBNull(reader["FirstName"]) ? null : (string)reader["FirstName"];
                                var authorLastName = Convert.IsDBNull(reader["LastName"]) ? null : (string)reader["LastName"];

                                if (authorFirstName != null && authorLastName != null)
                                {
                                    Author author = new Author(authorFirstName, authorLastName);
                                    author.AuthorID = (int)reader["AuthorId"];
                                    book.Authors.Add(author);
                                }

                                currentId = book.Id;

                                result.Add(book);

                                indexOfLast = result.IndexOf(book);
                            }
                        }
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.BookSQLDao)} Method: {nameof(DAL.SQL.BookSQLDao.GetAllSpecified)}");
                return null;
            }
        }
    }
}