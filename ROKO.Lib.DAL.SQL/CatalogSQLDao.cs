﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ROKO.Lib.DAL.SQL
{
    public class CatalogSQLDao : ICatalogDAO
    {
        private readonly IAuthorDAO _authorDAO;

        private readonly IBookDAO _bookDAO;
        private readonly ICatalogUnitDAO<Newspaper> _newspaperDAO;
        private readonly ICatalogUnitDAO<Patent> _patentDAO;

        private readonly ILogger<CatalogSQLDao> _logger;

        private readonly IConfiguration _configuration;

        public CatalogSQLDao(ILogger<CatalogSQLDao> logger)
        {
            _logger = logger;
        }

        public CatalogSQLDao(IConfiguration configuration, IAuthorDAO authorDAO, IBookDAO bookDAO, ICatalogUnitDAO<Newspaper> newspaperDAO, ICatalogUnitDAO<Patent> patentDAO, ILogger<CatalogSQLDao> logger)
        {
            _configuration = configuration;

            _authorDAO = authorDAO;

            _bookDAO = bookDAO;
            _newspaperDAO = newspaperDAO;
            _patentDAO = patentDAO;

            _logger = logger;
        }

        public IEnumerable<LibraryObject> GetCatalog()
        {
            var result = new List<LibraryObject>();

            try
            {
                var bookList = _bookDAO.GetAllSpecified();
                var newspaperList = _newspaperDAO.GetAllSpecified();
                var patentList = _patentDAO.GetAllSpecified();

                foreach (var item in bookList)
                {
                    result.Add(item);
                }

                foreach (var item in newspaperList)
                {
                    result.Add(item);
                }

                foreach (var item in patentList)
                {
                    result.Add(item);
                }

                return result.OrderBy(x => x.Id);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.CatalogSQLDao)} Method: {nameof(DAL.SQL.CatalogSQLDao.GetCatalog)}");
                return null;
            }
        }

        public bool Remove(int id)
        {
            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("RemoveLibraryObject", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterId = new SqlParameter("@Id", id);
                    command.Parameters.Add(parameterId);

                    connection.Open();

                    var reader = command.ExecuteNonQuery();
                }

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.CatalogSQLDao)} Method: {nameof(DAL.SQL.CatalogSQLDao.Remove)}");
                return false;
            }
        }

        public IEnumerable<LibraryObject> GetRecordByTitle(string title)
        {
            var result = new List<LibraryObject>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("[dbo].[SearchByTitle]", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterTitle = new SqlParameter("@title", title);
                    command.Parameters.Add(parameterTitle);

                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(
                                new LibraryObject(
                                    (string)reader["Title"],
                                    (int)reader["PublicationOrApplicationYear"],
                                    (int)reader["NumberOfPages"],
                                    (string)reader["Note"],
                                    (int)reader["LibraryObjectId"],
                                    (string)reader["LibraryObjectType"]
                                    ));
                        }
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.CatalogSQLDao)} Method: {nameof(DAL.SQL.CatalogSQLDao.GetRecordByTitle)}");
                return null;
            }
        }

        public IEnumerable<LibraryObject> Sort(SortingCriterion criterion)
        {
            return criterion.Equals(SortingCriterion.ASCPUBLICATIONYEAR) ? AscSort() : DescSort();
        }

        public IEnumerable<LibraryObject> AscSort()
        {
            var result = new List<LibraryObject>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("SortByPubDate", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(
                                new LibraryObject(
                                    (string)reader["Title"],
                                    (int)reader["PublicationOrApplicationYear"],
                                    (int)reader["NumberOfPages"],
                                    (string)reader["Note"],
                                    (int)reader["LibraryObjectId"],
                                    (string)reader["LibraryObjectType"]
                                    ));
                        }
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.CatalogSQLDao)} Method: {nameof(DAL.SQL.CatalogSQLDao.AscSort)}");
                return null;
            }
        }

        public IEnumerable<LibraryObject> DescSort()
        {
            var result = new List<LibraryObject>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("DescSortByPubDate", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(
                                new LibraryObject(
                                    (string)reader["Title"],
                                    (int)reader["PublicationOrApplicationYear"],
                                    (int)reader["NumberOfPages"],
                                    (string)reader["Note"],
                                    (int)reader["LibraryObjectId"],
                                    (string)reader["LibraryObjectType"]
                                    ));
                        }
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.CatalogSQLDao)} Method: {nameof(DAL.SQL.CatalogSQLDao.DescSort)}");
                return null;
            }
        }

        public IEnumerable<LibraryObject> FindBookByAuthor(Author author)
        {
            var result = new List<LibraryObject>();

            var bookIds = new List<int>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("SearchBookByAuthor", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterFirstName = new SqlParameter("@FirstName", author.FirstName);
                    command.Parameters.Add(parameterFirstName);

                    SqlParameter parameterLastName = new SqlParameter("@LastName", author.LastName);
                    command.Parameters.Add(parameterLastName);

                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var bookId = (int)reader["LibraryObjectId"];

                            bookIds.Add(bookId);
                        }
                    }
                }

                foreach (var bookId in bookIds)
                {
                    result.Add(FindCatalogElementById(bookId));
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.CatalogSQLDao)} Method: {nameof(DAL.SQL.CatalogSQLDao.FindBookByAuthor)}");
                return null;
            }
        }

        public LibraryObject FindCatalogElementById(int id)
        {
            var result = new LibraryObject();

            try
            {
                result = GetCatalog().Where(x => x.Id == id).FirstOrDefault();

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.CatalogSQLDao)} Method: {nameof(DAL.SQL.CatalogSQLDao.FindCatalogElementById)}");
                return null;
            }
        }

        public IEnumerable<LibraryObject> FindBooksAndPatentsOfAuthor(Author author)
        {
            var result = new List<LibraryObject>();

            var unitIds = new List<int>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("SearchBookAndPatentByAuthor", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterFirstName = new SqlParameter("@FirstName", author.FirstName);
                    command.Parameters.Add(parameterFirstName);

                    SqlParameter parameterLastName = new SqlParameter("@LastName", author.LastName);
                    command.Parameters.Add(parameterLastName);

                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var elementId = (int)reader["LibraryObjectId"];

                            unitIds.Add(elementId);
                        }
                    }
                }

                foreach (var unitId in unitIds)
                {
                    result.Add(FindCatalogElementById(unitId));
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.CatalogSQLDao)} Method: {nameof(DAL.SQL.CatalogSQLDao.FindBooksAndPatentsOfAuthor)}");
                return null;
            }
        }

        public IEnumerable<LibraryObject> FindPatentByAuthor(Author author)
        {
            var result = new List<LibraryObject>();

            var patentIds = new List<int>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("SearchPatentByAuthor", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterFirstName = new SqlParameter("@FirstName", author.FirstName);
                    command.Parameters.Add(parameterFirstName);

                    SqlParameter parameterLastName = new SqlParameter("@LastName", author.LastName);
                    command.Parameters.Add(parameterLastName);

                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var patentId = (int)reader["LibraryObjectId"];

                            patentIds.Add(patentId);
                        }
                    }
                }

                foreach (var patentId in patentIds)
                {
                    result.Add(FindCatalogElementById(patentId));
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.CatalogSQLDao)} Method: {nameof(DAL.SQL.CatalogSQLDao.FindPatentByAuthor)}");
                return null;
            }
        }

        public Dictionary<int, List<LibraryObject>> GroupRecordsByYear()
        {
            var result = new Dictionary<int, List<LibraryObject>>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("GetAllCatalog", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var temp =
                                new LibraryObject(
                                    (string)reader["Title"],
                                    (int)reader["PublicationOrApplicationYear"],
                                    (int)reader["NumberOfPages"],
                                    (string)reader["Note"],
                                    (int)reader["LibraryObjectId"],
                                    (string)reader["LibraryObjectType"]
                                    );

                            if (result.ContainsKey((int)temp.PublicationOrApplicationYear))
                            {
                                result[(int)temp.PublicationOrApplicationYear].Add(temp);
                            }
                            else
                            {
                                result.Add((int)temp.PublicationOrApplicationYear, new List<LibraryObject> { temp });
                            }
                        }
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.CatalogSQLDao)} Method: {nameof(DAL.SQL.CatalogSQLDao.GroupRecordsByYear)}");
                return null;
            }
        }

        public void SaveCatalogStorage()
        {
            throw new NotImplementedException();
        }
    }
}