﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;

using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ROKO.Lib.DAL.SQL
{
    public class UserSQLDao : IUserDAO
    {
        private readonly IConfiguration _configuration;

        private readonly ILogger<UserSQLDao> _logger;

        public UserSQLDao(IConfiguration configuration, ILogger<UserSQLDao> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public int AddUser(User user)
        {
            var userDbId = 0;

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("UsersConnection")))
                {
                    var command = new SqlCommand("AddUser", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterId = new SqlParameter("@Id", user.Id);
                    command.Parameters.Add(parameterId);

                    SqlParameter parameterLogin = new SqlParameter("@Login", user.Login);
                    command.Parameters.Add(parameterLogin);

                    SqlParameter parameterPassword = new SqlParameter("@Password", user.Password);
                    command.Parameters.Add(parameterPassword);

                    SqlParameter parameterRole = new SqlParameter("@Role", user.Role);
                    command.Parameters.Add(parameterRole);

                    connection.Open();

                    command.Parameters["@Id"].Direction = System.Data.ParameterDirection.Output;

                    var reader = command.ExecuteNonQuery();

                    userDbId = (int)command.Parameters["@Id"].Value;

                    command.Dispose();
                }

                return userDbId;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.UserSQLDao)} Method: {nameof(DAL.SQL.UserSQLDao.AddUser)}");
                return -1;
            }
        }

        public void DeleteUser(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetAll()
        {
            var result = new List<User>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("UsersConnection")))
                {
                    var command = new SqlCommand("GetAll", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            User user = new User(
                            (string)reader["Login"],
                            (string)reader["Password"],
                            (int)reader["Id"],
                            (string)reader["Role"]
                            );

                            result.Add(user);
                        }
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.UserSQLDao)} Method: {nameof(DAL.SQL.UserSQLDao.GetAll)}");
                return null;
            }
        }

        public User GetUserByLogin(string login)
        {
            User findedUser = new User();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("UsersConnection")))
                {
                    var command = new SqlCommand("GetUserByLogin", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterLogin = new SqlParameter("@Login", login);
                    command.Parameters.Add(parameterLogin);

                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            User user = new User(
                            (string)reader["Login"],
                            (string)reader["Password"],
                            (int)reader["Id"],
                            (string)reader["Role"]
                            );

                            findedUser = user;
                        }
                    }
                }

                return findedUser;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.UserSQLDao)} Method: {nameof(DAL.SQL.UserSQLDao.GetUserByLogin)}");
                return null;
            }
        }

        public void UpdateRole(User user)
        {
            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("UsersConnection")))
                {
                    var command = new SqlCommand("EditUserRole", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterId = new SqlParameter("@Id", user.Id);
                    command.Parameters.Add(parameterId);

                    SqlParameter parameterRole = new SqlParameter("@Role", user.Role);
                    command.Parameters.Add(parameterRole);

                    connection.Open();

                    var reader = command.ExecuteNonQuery();

                    command.Dispose();
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.UserSQLDao)} Method: {nameof(DAL.SQL.UserSQLDao.UpdateRole)}");
            }
        }
    }
}