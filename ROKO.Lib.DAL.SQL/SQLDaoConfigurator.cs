﻿using Microsoft.Extensions.DependencyInjection;

using ROKO.Lib.DAL.Interface;
using ROKO.Lib.DAL.SQL;
using ROKO.Lib.Entities;

namespace ROKO.Lib.DAL
{
    public static class SQLDaoConfigurator
    {
        public static IServiceCollection AddSQLDao(this IServiceCollection services)
        {
            return services
                           .AddTransient<ICatalogDAO, CatalogSQLDao>()
                           .AddTransient<IBookDAO, BookSQLDao>()
                           .AddTransient<ICatalogUnitDAO<Newspaper>, NewspaperSQLDao>()
                           .AddTransient<ICatalogUnitDAO<Patent>, PatentSQLDao>()
                           .AddTransient<IAuthorDAO, AuthorSQLDao>()
                           .AddTransient<IUserDAO, UserSQLDao>();
        }
    }
}