﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ROKO.Lib.DAL.SQL
{
    public class AuthorSQLDao : IAuthorDAO
    {
        private readonly IConfiguration _configuration;

        private readonly ILogger<AuthorSQLDao> _logger;

        public AuthorSQLDao(IConfiguration configuration, ILogger<AuthorSQLDao> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public CatalogUnitWithAuthors AddAuthors(CatalogUnitWithAuthors unit)
        {
            var authorId = -1;

            var authors = unit.Authors;

            List<Author> authorsListWithDBIds = new List<Author>();

            var authorsIds = new List<int>();

            try
            {
                foreach (var author in authors)
                {
                    using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                    {
                        var command = new SqlCommand("AddAuthor", connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;

                        SqlParameter parameterId = new SqlParameter("@AuthorId", author.AuthorID);
                        command.Parameters.Add(parameterId);

                        SqlParameter parameterFirstName = new SqlParameter("@FirstName", author.FirstName);
                        command.Parameters.Add(parameterFirstName);

                        SqlParameter parameterLastName = new SqlParameter("@LastName", author.LastName);
                        command.Parameters.Add(parameterLastName);

                        connection.Open();

                        command.Parameters["@AuthorId"].Direction = System.Data.ParameterDirection.Output;

                        var reader = command.ExecuteNonQuery();

                        authorId = (int)command.Parameters["@AuthorId"].Value;

                        authorsIds.Add(authorId);

                        authorsListWithDBIds.Add(new(authorId, author.FirstName, author.LastName));

                        command.Dispose();
                    }
                }

                unit.Authors = authorsListWithDBIds;

                return unit;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.AuthorSQLDao)} Method: {nameof(DAL.SQL.AuthorSQLDao.AddAuthors)}");
                return null;
            }
        }

        public void AddAuthorsToLibraryObject(int libraryObjectId, IEnumerable<int> authorsIds)
        {
            try
            {
                foreach (var authorId in authorsIds)
                {
                    using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                    {
                        var command = new SqlCommand("AddAuthorsToLibraryObjects", connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;

                        SqlParameter parameterAuthorId = new SqlParameter("@AuthorId", authorId);
                        command.Parameters.Add(parameterAuthorId);

                        SqlParameter parameterLibraryObjectId = new SqlParameter("@LibraryObjectId", libraryObjectId);
                        command.Parameters.Add(parameterLibraryObjectId);

                        connection.Open();

                        var reader = command.ExecuteNonQuery();

                        command.Dispose();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.AuthorSQLDao)} Method: {nameof(DAL.SQL.AuthorSQLDao.AddAuthorsToLibraryObject)}");
            }
        }

        public IEnumerable<Author> GetAuthors()
        {
            var result = new List<Author>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("GetAllAuthors", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Author author = new Author
                            (
                                (int)reader["AuthorId"],
                                (string)reader["FirstName"],
                                (string)reader["LastName"]
                            );

                            result.Add(author);
                        }
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.AuthorSQLDao)} Method: {nameof(DAL.SQL.AuthorSQLDao.GetAuthors)}");
                return null;
            }
        }

        public IEnumerable<Author> GetAuthorsByLibraryElementID(int ID)
        {
            var result = new List<Author>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("SelectAuthorsByLibraryElementID", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    command.Parameters.Add(new("@ID", ID));

                    connection.Open();

                    using var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Author author = new Author
                        (
                            (int)reader["AuthorId"],
                            (string)reader["FirstName"],
                            (string)reader["LastName"]
                        );

                        result.Add(author);
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.AuthorSQLDao)} Method: {nameof(DAL.SQL.AuthorSQLDao.GetAuthors)}");
                return null;
            }
        }
    }
}