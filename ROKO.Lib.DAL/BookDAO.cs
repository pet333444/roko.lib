﻿using Microsoft.Extensions.Logging;

using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;

using System.Collections.Generic;
using System.Linq;

namespace ROKO.Lib.DAL
{
    public class BookDAO : CatalogUnitDAO<Book>, IBookDAO
    {
        private readonly ILogger<BookDAO> _logger;

        public BookDAO(ILogger<BookDAO> logger) : base(logger)
        {
            _logger = logger;
        }

        public Dictionary<string, List<Book>> FindAllByPublisherNameTemplateAndGroupByPublisher(string publisherNameTemplate)
        {
            var temp = GetCatalog().Where(n => n is Book);

            Dictionary<string, List<Book>> res = new Dictionary<string, List<Book>>();

            List<Book> result = new List<Book>();

            foreach (var item in temp)
            {
                if (item.Title.StartsWith(publisherNameTemplate))
                {
                    result.Add(item as Book);
                }
            }

            foreach (var item in result)
            {
                if (!res.ContainsKey(item.PublishingHouse))
                {
                    res.Add(item.PublishingHouse, new List<Book>());
                }

                res[item.PublishingHouse].Add(item);
            }

            _logger.LogInformation($"Books found by Publisher Name and grouped.");
            return res;
        }
    }
}