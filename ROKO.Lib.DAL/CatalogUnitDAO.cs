﻿using Microsoft.Extensions.Logging;

using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;

using System;
using System.Collections.Generic;
using System.Linq;

namespace ROKO.Lib.DAL
{
    public class CatalogUnitDAO<T> : CatalogDAO, ICatalogUnitDAO<T> where T : LibraryObject
    {
        private readonly ILogger<CatalogUnitDAO<T>> _logger;

        public CatalogUnitDAO(ILogger<CatalogUnitDAO<T>> logger) : base(logger)
        {
            _logger = logger;
        }

        public int Add(T unit)
        {
            return base.Add(unit);
        }

        public bool Check(T input)
        {
            throw new NotImplementedException();
        }

        public void Edit(int id)
        {
            throw new NotImplementedException();
        }

        public void Edit(T unit)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> GetAllSpecified()
        {
            return base.GetCatalog().Where(x => x is T).Select(y => (T)y);
        }
    }
}