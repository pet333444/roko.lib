﻿using Microsoft.Extensions.Logging;

using Moq;

using NUnit.Framework;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.DAL;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.DAL.SQL;
using ROKO.Lib.Entities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ROKO.Lib.BLL.Tests
{
    [TestFixture]
    public class NewspaperLogicTest
    {
        private ICatalogLogic _catalogLogic;
        private ICatalogUnitLogic<Newspaper> _newspaperLogic;

        private IValidator<Newspaper> newspaperValidator;
        private IUniqueChecker<Newspaper> newspaperUniqueChecker;

        [SetUp]
        public void Startup()
        {
            var fakeLoggerCatalogDAO = new Mock<ILogger<CatalogSQLDao>>();
            var fakeLoggerNewspaperDAO = new Mock<ILogger<CatalogUnitDAO<Newspaper>>>();
            var fakeLoggerCatalogLogic = new Mock<ILogger<CatalogLogic>>();
            var fakeLoggerNewspaperValidator = new Mock<ILogger<NewspaperValidator>>();
            var fakeLoggerNewspaperUniqueChecker = new Mock<ILogger<NewspaperUniqueChecker>>();
            var fakeLoggerCatalogUnitLogic = new Mock<ILogger<CatalogUnitLogic<Newspaper>>>();

            var catalogDAO = new CatalogSQLDao(fakeLoggerCatalogDAO.Object);
            ICatalogUnitDAO<Newspaper> newspaperDAO = new CatalogUnitDAO<Newspaper>(fakeLoggerNewspaperDAO.Object);

            newspaperValidator = new NewspaperValidator(fakeLoggerNewspaperValidator.Object);
            newspaperUniqueChecker = new NewspaperUniqueChecker(newspaperDAO, fakeLoggerNewspaperUniqueChecker.Object);

            _newspaperLogic = new CatalogUnitLogic<Newspaper>(newspaperDAO, newspaperValidator, newspaperUniqueChecker, fakeLoggerCatalogUnitLogic.Object);
            _catalogLogic = new CatalogLogic(catalogDAO, fakeLoggerCatalogLogic.Object);
        }

        private static readonly int CorrectNumOfPages = 1,
                           IncorrectNumOfPages = -1,
                           CorrectYear = 2000,
                           IncorrectYear = 1;

        private static readonly string CorrectName = "CorrectName",
                              IncorrectName = "incorrect -458qqrname";

        private static readonly DateTime CorrectReleaseDate = new DateTime(CorrectYear, 1, 1),
                                IncorrectReleaseDate = new DateTime(IncorrectYear, 1, 1);

        [Test]
        public void NewspaperLogic_Add_WrongInputNewspaper_False()
        {
            Newspaper wrongNewspaper = new Newspaper(IncorrectName, IncorrectName, IncorrectName, IncorrectYear, IncorrectNumOfPages, IncorrectReleaseDate);

            Assert.AreEqual(_newspaperLogic.Add(wrongNewspaper), -1);
        }

        [Test]
        public void NewspaperLogic_Add_CorrectNewspaper_True()
        {
            Newspaper correctNewspaper = new Newspaper(CorrectName, CorrectName, CorrectName, CorrectYear, CorrectNumOfPages, CorrectReleaseDate);

            IEnumerable<Newspaper> temp = _newspaperLogic.GetAllSpecified();

            Assert.AreEqual(_newspaperLogic.Add(correctNewspaper), temp.Count());
        }

        [Test]
        public void NewspaperLogic_Add_SameObject_False()
        {
            Newspaper correctNewspaper = new Newspaper(CorrectName, CorrectName, CorrectName, CorrectYear, CorrectNumOfPages, CorrectReleaseDate);

            var res = _newspaperLogic.Add(correctNewspaper);
            res = _newspaperLogic.Add(correctNewspaper);

            Assert.AreEqual(res, -1);
        }

        [Test]
        public void NewspaperLogic_Remove_True()
        {
            Newspaper correctNewspaper = new Newspaper(CorrectName, CorrectName, CorrectName, CorrectYear, CorrectNumOfPages, CorrectReleaseDate);

            IEnumerable<Newspaper> temp = _newspaperLogic.GetAllSpecified();

            _newspaperLogic.Add(correctNewspaper);
            _catalogLogic.Remove(0);

            Assert.AreEqual(0, temp.Count());
        }

        [Test]
        public void NewspaperLogic_Add_PubPlaceRussianSymbols_True()
        {
            Newspaper correctNewspaper = new Newspaper("Title", "Москва", CorrectName, CorrectYear, CorrectNumOfPages, CorrectReleaseDate);

            Assert.IsTrue(_newspaperLogic.Add(correctNewspaper) > -1);
        }

        [Test]
        public void NewspaperLogic_Add_PubPlaceLatinSymbols_True()
        {
            Newspaper correctNewspaper = new Newspaper("Title", "New York", CorrectName, CorrectYear, CorrectNumOfPages, CorrectReleaseDate);

            Assert.IsTrue(_newspaperLogic.Add(correctNewspaper) > -1);
        }

        [Test]
        public void NewspaperLogic_Add_DoubleHyphen_False()
        {
            Newspaper incorrectNewspaper = new Newspaper("Title", "New-York-", CorrectName, CorrectYear, CorrectNumOfPages, CorrectReleaseDate);

            Assert.AreEqual(_newspaperLogic.Add(incorrectNewspaper), -1);
        }

        [Test]
        public void NewspaperLogic_Add_SingleHyphen_False()
        {
            Newspaper incorrectNewspaper = new Newspaper("Title", "-Волгоград", CorrectName, CorrectYear, CorrectNumOfPages, CorrectReleaseDate);

            Assert.AreEqual(_newspaperLogic.Add(incorrectNewspaper), -1);
        }

        [Test]
        public void NewspaperLogic_Add_PlaceOfPublicationTooLong_False()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 201; i++)
            {
                sb.Append('a');
            }

            var LongName = sb.ToString();

            Newspaper incorrectNewspaper = new Newspaper(CorrectName, LongName, CorrectName, CorrectYear, CorrectNumOfPages, CorrectReleaseDate);

            Assert.AreEqual(_newspaperLogic.Add(incorrectNewspaper), -1);
        }

        [Test]
        public void NewspaperLogic_Add_WrongNumberOfPages_False()
        {
            Newspaper incorrectNewspaper = new Newspaper(CorrectName, CorrectName, CorrectName, CorrectYear, IncorrectNumOfPages, CorrectReleaseDate);

            Assert.AreEqual(_newspaperLogic.Add(incorrectNewspaper), -1);
        }

        [Test]
        public void NewspaperLogic_Add_DifferentDates_False()
        {
            Newspaper incorrectNewspaper = new Newspaper(CorrectName, CorrectName, CorrectName, IncorrectYear, IncorrectNumOfPages, CorrectReleaseDate);

            Assert.AreEqual(_newspaperLogic.Add(incorrectNewspaper), -1);
        }
    }
}