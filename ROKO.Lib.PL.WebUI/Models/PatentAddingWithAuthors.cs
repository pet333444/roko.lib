﻿using ROKO.Lib.Entities;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ROKO.Lib.PL.WebUI.Models
{
    public class PatentAddingWithAuthors
    {
        public Patent Patent { get; set; }

        [Required]
        public IEnumerable<Author> Authors { get; set; }
    }
}