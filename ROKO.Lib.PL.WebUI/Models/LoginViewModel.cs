﻿using System.ComponentModel.DataAnnotations;

namespace ROKO.Lib.PL.WebUI.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Не указан Login")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Не указан Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}