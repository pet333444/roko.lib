﻿using ROKO.Lib.Entities;

using System.Collections.Generic;

namespace ROKO.Lib.PL.WebUI.Models
{
    public class NewspaperViewModel
    {
        public Newspaper CurrentNewspaper { get; set; }

        public IEnumerable<Newspaper> NewspaperIssues { get; set; }
    }
}