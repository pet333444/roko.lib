﻿using ROKO.Lib.Entities;

using System.Collections.Generic;

namespace ROKO.Lib.PL.WebUI.Models
{
    public class CatalogIndexViewModel
    {
        public IEnumerable<NewCatalogRequirementsViewModel> CatalogElements { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}