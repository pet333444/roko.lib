﻿using System.ComponentModel.DataAnnotations;

namespace ROKO.Lib.PL.WebUI.Models
{
    public class NewCatalogRequirementsViewModel
    {
        [ScaffoldColumn(false)]
        public int UnitId { get; set; }

        [Display(Name = "Наименование")]
        public string Name { get; set; }

        [Display(Name = "Идентификатор")]
        public string Identificator { get; set; }

        [Display(Name = "Количество страниц")]
        public int NumberOfPages { get; set; }
    }
}