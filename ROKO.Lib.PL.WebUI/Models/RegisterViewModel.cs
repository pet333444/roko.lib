﻿using ROKO.Lib.PL.WebUI.ValidationAttributes;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace ROKO.Lib.PL.WebUI.Models
{
    [LoginPasswordEqual]
    public class RegisterViewModel : IValidatableObject
    {
        [Required(ErrorMessage = "Не указан Login")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Не указан Password")]
        [DataType(DataType.Password)]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина пароля должна быть не менее 3 символов")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string ConfirmPassword { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            Regex regex = new Regex(@"^([A-Za-z0-9_\s]*)$");

            if (this.Login.StartsWith('_') || this.Login.EndsWith('_'))
            {
                errors.Add(new ValidationResult("Логин не может начинаться или заканчиваться знаком подчёркивания!"));
            }
            if (char.IsDigit(this.Login[0]))
            {
                errors.Add(new ValidationResult("Логин не может начинаться с цифры!"));
            }
            if (!regex.IsMatch(this.Login))
            {
                errors.Add(new ValidationResult("Логин может включать в себя только латинские символы основного набора (A-Z), цифры и знаки подчёркивания!"));
            }

            for (int i = 0; i < this.Password.Length; i++)
            {
                if (this.Password[i] > (char)0000 && this.Password[i] < (char)001F)
                {
                    errors.Add(new ValidationResult("Пароль не может содержать управляющие символы Unicode с кодами меньше 32!"));
                }
            }

            for (int i = 0; i < this.Login.Length - 2; i++)
            {
                if (this.Login[i] == '_' && this.Login[i + 1] == '_')
                {
                    errors.Add(new ValidationResult("Знаки подчёркивания не могут идти подряд!"));
                }
            }

            return errors;
        }
    }
}