﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.Entities;

using System;
using System.Collections.Generic;
using System.Linq;

namespace ROKO.Lib.PL.WebUI.Controllers
{
    public class BookController : Controller
    {
        private readonly IBookLogic _bookLogic;
        private readonly IAuthorLogic _authorLogic;
        private readonly ICatalogLogic _catalogLogic;

        private readonly ILogger<BookController> _logger;

        public BookController(IBookLogic bookLogic,
                              ICatalogLogic catalogLogic,
                              IAuthorLogic authorLogic, ILogger<BookController> logger)
        {
            _bookLogic = bookLogic;
            _authorLogic = authorLogic;
            _catalogLogic = catalogLogic;
            _logger = logger;
        }

        // GET: BookController
        public ActionResult Index()
        {
            IEnumerable<Book> books = _bookLogic.GetAllSpecified();
            return View(books);
        }

        // GET: BookController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: BookController/Create
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            ViewBag.authors = _authorLogic.GetAuthors().Select(x => new SelectListItem { Value = x.AuthorID.ToString(), Text = x.FirstName + " " + x.LastName }).ToList();

            _logger.LogInformation($"Message: Admin with login '{User.Identity.Name}' opened Book Creating view. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(BookController)} Method: {nameof(BookController.Create)}");

            return View("CreateBookWithAuthors");
        }

        // POST: BookController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Create(Book book, IEnumerable<int> authors)
        {
            try
            {
                var bookId = _bookLogic.Add(book);

                _authorLogic.AddAuthorsToLibraryObject(bookId, authors);

                _logger.LogInformation($"Message: Admin with login '{User.Identity.Name}' added new book with id = {bookId}. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(BookController)} Method: {nameof(BookController.Create)}");

                return RedirectToAction("Index", "Catalog");
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} CurrentUser: {User.Identity.Name} Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(BookController)} Method: {nameof(BookController.Create)}");

                return RedirectToAction("Create");
            }
        }

        // GET: BookController/Edit/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: BookController/Edit/5
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Book book)
        {
            try
            {
                _bookLogic.Edit(book);

                _logger.LogInformation($"Message: Admin edit book with id = {book.Id}. CurrentUser: {User.Identity.Name} Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(CatalogController)} Method: {nameof(CatalogController.Edit)}");

                return RedirectToAction(nameof(Index), "Catalog");
            }
            catch
            {
                return View();
            }
        }

        // GET: BookController/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: BookController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}