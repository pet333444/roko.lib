﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.Entities;

using System;
using System.Linq;

namespace ROKO.Lib.PL.WebUI.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {
        private readonly IUserLogic _userLogic;

        private readonly ILogger<AdminController> _logger;

        public AdminController(IUserLogic userLogic, ILogger<AdminController> logger)
        {
            _userLogic = userLogic;
            _logger = logger;
        }

        public ActionResult Index()
        {
            _logger.LogInformation($"Message: Admin with login '{User.Identity.Name}' opened UsersIndex. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(AdminController)} Method: {nameof(AdminController.Index)}");
            return View("UsersIndex", _userLogic.GetAll());
        }

        // GET: AdminController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AdminController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: AdminController/Edit/5
        public ActionResult Edit(int id)
        {
            _logger.LogInformation($"Message: Admin with login '{User.Identity.Name}' opened EditUserRole view. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(AdminController)} Method: {nameof(AdminController.Edit)}");
            return View("EditUserRole", _userLogic.GetAll().Where(x => x.Id == id).FirstOrDefault());
        }

        // POST: AdminController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, User user)
        {
            try
            {
                _userLogic.UpdateRole(user);
                _logger.LogInformation($"Message: Admin with login '{User.Identity.Name}' updated role of user with id = {user.Id}. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(AdminController)} Method: {nameof(AdminController.Edit)}");
                return RedirectToAction("Index", "Admin");
            }
            catch
            {
                return View();
            }
        }

        // GET: AdminController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AdminController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}