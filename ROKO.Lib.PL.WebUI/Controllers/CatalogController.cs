﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.Entities;
using ROKO.Lib.PL.WebUI.Models;

using System;
using System.Collections.Generic;
using System.Linq;

namespace ROKO.Lib.PL.WebUI.Controllers
{
    public class CatalogController : Controller
    {
        private readonly ICatalogLogic _catalogLogic;

        private readonly IBookLogic _bookLogic;
        private readonly ICatalogUnitLogic<Newspaper> _newspaperLogic;
        private readonly ICatalogUnitLogic<Patent> _patentLogic;

        private readonly ILogger<CatalogController> _logger;

        public CatalogController(ICatalogLogic catalogLogic, IBookLogic bookLogic, ICatalogUnitLogic<Newspaper> newspaperLogic, ICatalogUnitLogic<Patent> patentLogic, ILogger<CatalogController> logger)
        {
            _catalogLogic = catalogLogic;

            _bookLogic = bookLogic;
            _newspaperLogic = newspaperLogic;
            _patentLogic = patentLogic;

            _logger = logger;
        }

        // GET: CatalogController
        public ActionResult Index(int page = 1)
        {
            IEnumerable<LibraryObject> catalog = _catalogLogic.GetCatalog();

            List<NewCatalogRequirementsViewModel> list = new();

            foreach (var item in catalog)
            {
                NewCatalogRequirementsViewModel model = new NewCatalogRequirementsViewModel();

                if (item.Type == "Book")
                {
                    Book book = item as Book;
                    model.UnitId = book.Id;

                    if (book.Authors.Count == 0)
                    {
                        model.Name = $"{book.Title}({book.PublicationOrApplicationYear})";
                    }
                    else
                    {
                        model.Name = $"{book.AuthorList()} - {book.Title}({book.PublicationOrApplicationYear})";
                    }

                    model.Identificator = book.ISBN;
                    model.NumberOfPages = book.NumberOfPages;
                }

                if (item.Type == "Newspaper")
                {
                    Newspaper newspaper = item as Newspaper;
                    model.UnitId = newspaper.Id;

                    if (newspaper.IssueNumber == -1)
                    {
                        model.Name = $"{newspaper.Title} {newspaper.PublicationOrApplicationYear}";
                    }
                    else
                    {
                        model.Name = $"{newspaper.Title} №{newspaper.IssueNumber}/{newspaper.PublicationOrApplicationYear}";
                    }

                    model.Identificator = newspaper.ISSN;
                    model.NumberOfPages = newspaper.NumberOfPages;
                }

                if (item.Type == "Patent")
                {
                    Patent patent = item as Patent;
                    model.UnitId = patent.Id;
                    model.Name = $"«{patent.Title}» от {patent.PublicationDate:dd.MM.yyyy}";
                    model.Identificator = patent.RegistrationNumber.ToString();
                    model.NumberOfPages = patent.NumberOfPages;
                }

                list.Add(model);
            }

            int pageSize = 20;

            IEnumerable<NewCatalogRequirementsViewModel> elementsPerPages = list.Skip((page - 1) * pageSize).Take(pageSize);
            PageInfo pageInfo = new PageInfo(list.Count, page, pageSize);
            CatalogIndexViewModel catalogIndexView = new() { PageInfo = pageInfo, CatalogElements = elementsPerPages };

            return View("PagedIndex", catalogIndexView);
        }

        // GET: CatalogController/Details/5
        public ActionResult Details(int id)
        {
            var findedElement = _catalogLogic.FindCatalogElementById(id);

            if (findedElement.Type == "Book")
            {
                return View("BookDetails", findedElement);
            }

            if (findedElement.Type == "Newspaper")
            {
                return RedirectToAction("Details", "Newspaper", new { id });
            }

            if (findedElement.Type == "Patent")
            {
                return View("PatentDetails", findedElement);
            }

            //Error View
            return View("EditErrorGet");
        }

        // GET: CatalogController/Create
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: CatalogController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CatalogController/Edit/5
        [Authorize(Roles = "Administrator")]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id)
        {
            var findedElement = _catalogLogic.FindCatalogElementById(id);

            if (findedElement.Type == "Book")
            {
                _logger.LogInformation($"Message: Admin opened Book Editing view with id = {id}. CurrentUser: {User.Identity.Name} Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(CatalogController)} Method: {nameof(CatalogController.Edit)}");

                return View("EditBookGet", findedElement);
            }

            if (findedElement.Type == "Newspaper")
            {
                _logger.LogInformation($"Message: Admin opened Newspaper Editing view with id = {id}. CurrentUser: {User.Identity.Name} Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(CatalogController)} Method: {nameof(CatalogController.Edit)}");

                return View("EditNewspaperGet", findedElement);
            }

            if (findedElement.Type == "Patent")
            {
                _logger.LogInformation($"Message: Admin opened Patent Editing view with id = {id}. CurrentUser: {User.Identity.Name} Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(CatalogController)} Method: {nameof(CatalogController.Edit)}");

                return View("EditPatentGet", findedElement);
            }

            //Error View
            return View("EditErrorGet");
        }

        // POST: CatalogController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id, LibraryObject libraryObject)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View("EditErrorGet");
            }
        }

        // GET: CatalogController/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int id)
        {
            var findedElement = _catalogLogic.FindCatalogElementById(id);

            if (findedElement.Type == "Book")
            {
                _logger.LogInformation($"Message: Admin opened Book Deleting view with id = {id}. CurrentUser: {User.Identity.Name} Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(CatalogController)} Method: {nameof(CatalogController.Delete)}");

                return View("DeleteBook", findedElement);
            }

            if (findedElement.Type == "Newspaper")
            {
                _logger.LogInformation($"Message: Admin opened Newspaper Deleting view with id = {id}. CurrentUser: {User.Identity.Name} Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(CatalogController)} Method: {nameof(CatalogController.Delete)}");

                return View("DeleteNewspaper", findedElement);
            }

            if (findedElement.Type == "Patent")
            {
                _logger.LogInformation($"Message: Admin opened Patent Deleting view with id = {id}. CurrentUser: {User.Identity.Name} Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(CatalogController)} Method: {nameof(CatalogController.Delete)}");

                return View("DeletePatent", findedElement);
            }

            //Error View
            return View("EditErrorGet");
        }

        // POST: CatalogController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                _catalogLogic.Remove(id);

                _logger.LogInformation($"Message: Admin deleted element of catalog with id = {id}. CurrentUser: {User.Identity.Name} Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(CatalogController)} Method: {nameof(CatalogController.Delete)}");

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}