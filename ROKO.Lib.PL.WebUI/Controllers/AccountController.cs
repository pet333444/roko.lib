﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.Entities;
using ROKO.Lib.PL.WebUI.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace ROKO.Lib.PL.WebUI.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserLogic _userLogic;

        private readonly ILogger<AccountController> _logger;

        public AccountController(IUserLogic userLogic, ILogger<AccountController> logger)
        {
            _userLogic = userLogic;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Login(string returnUrl)
        {
            TempData["returnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginViewModel userCredentials)
        {
            string hash = "";

            using (SHA512 sha512Hash = SHA512.Create())
            {
                byte[] sourceBytes = Encoding.Unicode.GetBytes(userCredentials.Password);
                byte[] hashBytes = sha512Hash.ComputeHash(sourceBytes);
                hash = BitConverter.ToString(hashBytes).Replace("-", string.Empty);
            }

            if (ModelState.IsValid)
            {
                User user = _userLogic.GetUserByLogin(userCredentials.Login);

                if (user != null && user.Password == hash)
                {
                    Authenticate(user);

                    if (TempData["returnUrl"] != null)
                    {
                        return Redirect(TempData["returnUrl"].ToString());
                    }
                    else
                    {
                        return Redirect("/Home/Index");
                    }
                }
            }

            ViewBag.ErrorMessage = "Incorrect login or password!";

            _logger.LogInformation($"Message: Login attempt with login '{userCredentials.Login}' failed. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(PL.WebUI.Controllers.AccountController)} Method: {nameof(PL.WebUI.Controllers.AccountController.Login)}");

            return View();
        }

        private void Authenticate(User user)
        {
            var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, user.Login),
                        new Claim(ClaimTypes.Role, user.Role)
                    };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties() { };

            HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), authProperties);

            _logger.LogInformation($"Message: User '{user.Login}' authenticated. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(PL.WebUI.Controllers.AccountController)} Method: {nameof(PL.WebUI.Controllers.AccountController.Authenticate)}");
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Register(RegisterViewModel registerViewModel)
        {
            if (ModelState.IsValid)
            {
                User user = _userLogic.GetAll().FirstOrDefault(x => x.Login == registerViewModel.Login);//GetUserByLogin(registerViewModel.Login);

                if (user == null)
                {
                    using (SHA512 sha512Hash = SHA512.Create())
                    {
                        byte[] sourceBytes = Encoding.Unicode.GetBytes(registerViewModel.Password);
                        byte[] hashBytes = sha512Hash.ComputeHash(sourceBytes);
                        string hash = BitConverter.ToString(hashBytes).Replace("-", String.Empty);

                        user = new User { Login = registerViewModel.Login, Password = hash, Role = "User" };
                    }

                    _userLogic.AddUser(user);

                    Authenticate(user);

                    _logger.LogInformation($"Message: Registration with login '{registerViewModel.Login}' successful. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(PL.WebUI.Controllers.AccountController)} Method: {nameof(PL.WebUI.Controllers.AccountController.Register)}");

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    _logger.LogError($"ErrorMessage: Registration with login like existing. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(PL.WebUI.Controllers.AccountController)} Method: {nameof(PL.WebUI.Controllers.AccountController.Register)}");
                    ModelState.AddModelError("", "Пользователь с таким логином уже существует!");
                }
            }

            return View();
        }

        [Authorize]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            _logger.LogInformation($"Message: User '{User.Identity.Name}' logout. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(PL.WebUI.Controllers.AccountController)} Method: {nameof(PL.WebUI.Controllers.AccountController.Logout)}");

            return RedirectToAction("Login", "Account");
        }
    }
}