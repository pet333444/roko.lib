﻿using System.ComponentModel.DataAnnotations;

namespace ROKO.Lib.Entities
{
    public class User
    {
        public int Id { get; set; }

        [Required]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string Role { get; set; }

        public User()
        {
        }

        public User(string login, string password, int id = 0, string role = "user")
        {
            Id = id;
            Login = login;
            Password = password;
            Role = role;
        }
    }
}