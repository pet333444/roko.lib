﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ROKO.Lib.Entities
{
    public class Newspaper : LibraryObject
    {
        [Required]
        [MaxLength(200, ErrorMessage = "Длина поля не должна превышать 200 символов!")]
        public string PlaceOfPublication { get; set; }

        [Required]
        [MaxLength(300, ErrorMessage = "Длина поля не должна превышать 300 символов!")]
        public string PublishingHouse { get; set; }

        public int IssueNumber { get; set; }

        [Required]
        public DateTime ReleaseDate { get; set; }

        [RegularExpression(@"\b(ISSN)\b\s[0-9]{4}-[0-9]{4}", ErrorMessage = "ISSN должен удовлетворять формату записи!")]
        public string ISSN { get; set; }

        public Newspaper()
        {
        }

        public Newspaper(string title, string placeOfPub, string pubHouse, int pubYear, int numOfPages,
             DateTime releaseDate, string note = "", int issueNumber = -1, string issn = "")
            : base(title, pubYear, numOfPages, note)
        {
            Title = title;
            PlaceOfPublication = placeOfPub;
            PublishingHouse = pubHouse;
            PublicationOrApplicationYear = pubYear;
            NumberOfPages = numOfPages;
            Note = note;
            IssueNumber = issueNumber;
            ReleaseDate = releaseDate;
            ISSN = issn;
        }

        public override string ToString()
        {
            return $"Id: {Id} | Type: {Type} | Title: {Title} | PlaceOfPublication: {PlaceOfPublication} | PublishingHouse: {PublishingHouse} | PublicationYear: {PublicationOrApplicationYear} | NumberOfPages: {NumberOfPages} | ReleaseDate: {ReleaseDate.ToShortDateString()} | Note: {Note} | IssueNumber: {IssueNumber} | ISSN: {ISSN} ";
        }
    }
}