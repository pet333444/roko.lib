﻿namespace ROKO.Lib.Entities
{
    public class Author
    {
        public Author(string fname, string lname)
        {
            FirstName = fname;
            LastName = lname;
        }

        public Author()
        {
        }

        public Author(int id, string fname, string lname)
        {
            AuthorID = id;
            FirstName = fname;
            LastName = lname;
        }

        public int AuthorID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public override string ToString()
        {
            return $"{FirstName} {LastName} ";
        }
    }
}