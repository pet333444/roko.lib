﻿namespace ROKO.Lib.Entities
{
    public enum SortingCriterion
    {
        ASCPUBLICATIONYEAR = 0,
        DESCPUBLICATIONYEAR = 1
    }
}