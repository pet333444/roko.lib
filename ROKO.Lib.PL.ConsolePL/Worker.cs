﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.Entities;

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ROKO.Lib.PL.ConsolePL
{
    public class Worker : BackgroundService
    {
        private readonly ICatalogLogic _catalogLogic;
        private readonly IBookLogic _bookLogic;
        private readonly ICatalogUnitLogic<Newspaper> _newspaperLogic;
        private readonly ICatalogUnitLogic<Patent> _patentLogic;
        private readonly IAuthorLogic _authorLogic;

        private readonly ILogger<Worker> _logger;

        public Worker(ICatalogLogic catalogLogic, IBookLogic bookLogic, ICatalogUnitLogic<Newspaper> newspaperLogic, ICatalogUnitLogic<Patent> patentLogic, IAuthorLogic authorLogic, ILogger<Worker> logger)
        {
            _catalogLogic = catalogLogic;
            _bookLogic = bookLogic;
            _newspaperLogic = newspaperLogic;
            _patentLogic = patentLogic;

            _authorLogic = authorLogic;

            _logger = logger;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            string inp_key = "default";

            Console.WriteLine($"Keys:{Environment.NewLine}" +
                              $"Stop - S{Environment.NewLine}" +
                              $"Add Book - AB{Environment.NewLine}" +
                              $"Add Newspaper - AN{Environment.NewLine}" +
                              $"Add Patent - AP{Environment.NewLine}" +
                              $"Edit Book - EB{Environment.NewLine}" +
                              $"Edit Newspaper - EN{Environment.NewLine}" +
                              $"Edit Patent - EP{Environment.NewLine}" +
                              $"Get all - G{Environment.NewLine}" +
                              $"Remove - R{Environment.NewLine}" +
                              $"Search By Title - SBT{Environment.NewLine}" +
                              $"Sort - SR{Environment.NewLine}" +
                              $"Search Book by Authors - SBA{Environment.NewLine}" +
                              $"Search Patent by Authors - SPA{Environment.NewLine}" +
                              $"Search Book&Patent by Authors - SBPA{Environment.NewLine}" +
                              $"PubHouse Starts With and Group By PubHouse - PHS{Environment.NewLine}" +
                              $"Group by year - GBY{Environment.NewLine}");

            while (inp_key != "S")
            {
                Console.WriteLine("Enter key: ");
                ReadKey(ref inp_key);
                switch (inp_key)
                {
                    case "AB":
                        {
                            AddBook(_bookLogic, _authorLogic);
                            break;
                        }

                    case "AN":
                        {
                            AddNewsPaper(_newspaperLogic);
                            break;
                        }

                    case "AP":
                        {
                            AddPatent(_patentLogic, _authorLogic);
                            break;
                        }

                    case "EB":
                        {
                            EditBook(_bookLogic, _catalogLogic);
                            break;
                        }

                    case "EN":
                        {
                            EditNewspaper(_newspaperLogic, _catalogLogic);
                            break;
                        }

                    case "EP":
                        {
                            EditPatent(_patentLogic, _catalogLogic);
                            break;
                        }

                    case "G":
                        {
                            GetAll(_catalogLogic);
                            break;
                        }

                    case "R":
                        {
                            RemoveUnit(_catalogLogic);
                            break;
                        }

                    case "SBT":
                        {
                            SearchByTitle(_catalogLogic);
                            break;
                        }

                    case "SR":
                        {
                            Sort(_catalogLogic);
                            break;
                        }

                    case "SBA":
                        {
                            SearchBookByAuthor(_catalogLogic);
                            break;
                        }

                    case "SPA":
                        {
                            SearchPatentByAuthor(_catalogLogic);
                            break;
                        }

                    case "SBPA":
                        {
                            SearchBookAndPatentByAuthor(_catalogLogic);
                            break;
                        }

                    case "PHS":
                        {
                            PubHouseStartsWith(_bookLogic);
                            break;
                        }

                    case "GBY":
                        {
                            GroupByPubYear(_catalogLogic);
                            break;
                        }

                    case "S":
                        {
                            break;
                        }

                    default:
                        {
                            Console.WriteLine("You have entered wrong input data. Try again.");
                        }

                        break;
                }

                Console.WriteLine("____________");
            }

            return Task.CompletedTask;
        }

        private void EditPatent(ICatalogUnitLogic<Patent> patentLogic, ICatalogLogic catalogLogic)
        {
            try
            {
                Console.WriteLine("Enter Id of patent you want to edit:");
                int id = int.Parse(Console.ReadLine());

                var oldPatent = (Patent)catalogLogic.FindCatalogElementById(id);
                Console.WriteLine(oldPatent.ToString());

                Console.Write("Enter New Title: ");
                string title = Console.ReadLine();
                //Console.Write("Enter New number of Inventors: ");
                //int numOfInventors = int.Parse(Console.ReadLine());
                Console.Write("Enter New country: ");
                string country = Console.ReadLine();
                Console.Write("Enter New registration number: ");
                int regNumber = int.Parse(Console.ReadLine());
                Console.Write("Enter New application date(DateTime format): ");
                DateTime? appDate = DateTime.Parse(Console.ReadLine());
                Console.Write("Enter New publication date: ");
                DateTime pubDate = DateTime.Parse(Console.ReadLine());
                Console.Write("Enter New number of pages: ");
                int numOfPages = int.Parse(Console.ReadLine());
                Console.Write("Enter New Note: ");
                string note = Console.ReadLine();

                var newPatent = new Patent(title, country, regNumber, pubDate, numOfPages, appDate, oldPatent.Authors, note);
                newPatent.Id = oldPatent.Id;

                patentLogic.Edit(newPatent);

                //if (patentId == -1)
                //{
                //    Console.WriteLine($"--------ERROR #3. Wrong input Book data!--------{Environment.NewLine}Information was not saved.");
                //}
                //else
                //{
                //    authorLogic.AddAuthorsToLibraryObject(patentId, authorsIdsList);
                //}
            }
            catch
            {
                Console.WriteLine("You have entered the wrong input data. Try again.");
            }
        }

        private static void EditNewspaper(ICatalogUnitLogic<Newspaper> newspaperLogic, ICatalogLogic catalogLogic)
        {
            try
            {
                Console.WriteLine("Enter Id of newspaper you want to edit:");
                int id = int.Parse(Console.ReadLine());

                var oldNewspaper = (Newspaper)catalogLogic.FindCatalogElementById(id);
                Console.WriteLine(oldNewspaper.ToString());

                Console.Write("Enter New title: ");
                string title = Console.ReadLine();
                Console.Write("Enter New publishing place: ");
                string pubPlace = Console.ReadLine();
                Console.Write("Enter New publishing house: ");
                string pubHouse = Console.ReadLine();
                Console.Write("Enter New publishing year: ");
                int pubYear = int.Parse(Console.ReadLine());
                Console.Write("Enter New number of pages: ");
                int numOfpages = int.Parse(Console.ReadLine());
                Console.Write("Enter New Release Date(DateTime format): ");
                DateTime releaseDate = DateTime.Parse(Console.ReadLine());
                Console.Write("Enter New Note: ");
                string note = Console.ReadLine();
                Console.Write("Enter New Issue Number: ");
                int issueNumber = int.Parse(Console.ReadLine());
                Console.Write("Enter New ISSN: ");
                string issn = Console.ReadLine();

                var newNewspaper = new Newspaper(title, pubPlace, pubHouse, pubYear, numOfpages, releaseDate, note, issueNumber, issn);
                newNewspaper.Id = oldNewspaper.Id;

                newspaperLogic.Edit(newNewspaper);

                //if (newspaperLogic.Add(newspaper) == -1)
                //{
                //    Console.WriteLine($"--------ERROR #2. Wrong input Newspaper data!--------{Environment.NewLine}Infornation was not saved.");
                //}
            }
            catch
            {
                Console.WriteLine("You have entered the wrong input data. Try again.");
            }
        }

        private static void EditBook(IBookLogic bookLogic, ICatalogLogic catalogLogic)
        {
            try
            {
                Console.WriteLine("Enter Id of book you want to edit:");
                int id = int.Parse(Console.ReadLine());

                var oldBook = (Book)catalogLogic.FindCatalogElementById(id);
                Console.WriteLine(oldBook.ToString());

                Console.Write("Enter New Title Of Book: ");
                string title = Console.ReadLine();
                Console.Write("Enter New Place Of Publication: ");
                string pubPlace = Console.ReadLine();
                Console.Write("Enter New Publishing House: ");
                string pubHouse = Console.ReadLine();
                Console.Write("Enter New Publishing Year: ");
                int pubDate = int.Parse(Console.ReadLine());
                Console.Write("Enter New Number of Pages: ");
                int countOfpages = int.Parse(Console.ReadLine());
                Console.Write("Enter New Note: ");
                string note = Console.ReadLine();
                Console.Write("Enter New ISBN: ");
                string isbn = Console.ReadLine();

                //Console.WriteLine("If you want to add author(s), press key Y. Else press any key.");
                //List<Authors> authors = new List<Authors>(0);
                //if (Console.ReadKey().Key == ConsoleKey.Y)
                //{
                //    Console.Write("Enter number of Authors: ");
                //    int numOfAuthors = int.Parse(Console.ReadLine());

                //    for (int i = 0; i < numOfAuthors; i++)
                //    {
                //        string fname, lname;
                //        Console.Write("Enter First Name of Authors: ");
                //        fname = Console.ReadLine();

                //        Console.Write("Enter Last Name of Authors: ");
                //        lname = Console.ReadLine();

                //        authors.Add(new Authors(fname, lname));
                //    }
                //}

                //var book = new Book();

                var newBook = new Book(title, pubPlace, pubHouse, pubDate, countOfpages, oldBook.Authors, note, isbn);
                newBook.Id = oldBook.Id;

                bookLogic.Edit(newBook);

                //if (bookId == -1)
                //{
                //    Console.WriteLine($"--------ERROR #1. Wrong input Book data!--------{Environment.NewLine}Information was not saved.");
                //}
            }
            catch
            {
                Console.WriteLine("You have entered the wrong input data. Try again.");
            }
        }

        public static void GetAll(ICatalogLogic catalogLogic)
        {
            Console.WriteLine();
            Console.WriteLine("_______________________________________________________");
            IEnumerable<LibraryObject> result = catalogLogic.GetCatalog();
            foreach (LibraryObject item in result)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine("_______________________________________________________");
        }

        public static void AddBook(IBookLogic bookLogic, IAuthorLogic authorLogic)
        {
            try
            {
                Console.Write("Enter Title Of Book: ");
                string title = Console.ReadLine();
                Console.Write("Enter Place Of Publication: ");
                string pubPlace = Console.ReadLine();
                Console.Write("Enter Publishing House: ");
                string pubHouse = Console.ReadLine();
                Console.Write("Enter Publishing Year: ");
                int pubDate = int.Parse(Console.ReadLine());
                Console.Write("Enter Number of Pages: ");
                int countOfpages = int.Parse(Console.ReadLine());

                Console.WriteLine("If you want to add author(s), note and ISBN, press key Y. Else press any key.");
                string note = "", isbn = "";
                List<Author> authors = new List<Author>(0);
                if (Console.ReadKey().Key == ConsoleKey.Y)
                {
                    Console.Write("Enter number of Authors: ");
                    int numOfAuthors = int.Parse(Console.ReadLine());

                    for (int i = 0; i < numOfAuthors; i++)
                    {
                        string fname, lname;
                        Console.Write("Enter First Name of Authors: ");
                        fname = Console.ReadLine();

                        Console.Write("Enter Last Name of Authors: ");
                        lname = Console.ReadLine();

                        authors.Add(new Author(fname, lname));
                    }

                    Console.Write("Enter Note: ");
                    note = Console.ReadLine();

                    Console.Write("Enter ISBN: ");
                    isbn = Console.ReadLine();
                }

                //var book = new Book();

                var book = new Book(title, pubPlace, pubHouse, pubDate, countOfpages, authors, note, isbn);

                List<int> authorsIdsList = new List<int>();

                if (book.Authors.Count != 0)
                {
                    var bookWithAuthors = (Book)authorLogic.AddAuthors(book);

                    foreach (var item in bookWithAuthors.Authors)
                    {
                        authorsIdsList.Add(item.AuthorID);
                    }
                }

                var bookId = bookLogic.Add(book);

                if (bookId == -1)
                {
                    Console.WriteLine($"--------ERROR #1. Wrong input Book data!--------{Environment.NewLine}Information was not saved.");
                }
                else
                {
                    authorLogic.AddAuthorsToLibraryObject(bookId, authorsIdsList);
                }
            }
            catch
            {
                Console.WriteLine("You have entered the wrong input data. Try again.");
            }
        }

        public static void AddNewsPaper(ICatalogUnitLogic<Newspaper> newspaperLogic)
        {
            try
            {
                Console.Write("Enter title: ");
                string title = Console.ReadLine();
                Console.Write("Enter publishing place: ");
                string pubPlace = Console.ReadLine();
                Console.Write("Enter publishing house: ");
                string pubHouse = Console.ReadLine();
                Console.Write("Enter publishing year: ");
                int pubYear = int.Parse(Console.ReadLine());
                Console.Write("Enter number of pages: ");
                int numOfpages = int.Parse(Console.ReadLine());
                Console.Write("Enter Release Date(DateTime format): ");
                DateTime releaseDate = DateTime.Parse(Console.ReadLine());

                Console.WriteLine("If you want to add Note, Issue Number or ISSN to newspaper, press key Y. Else press any key.");
                string note = "", issn = "";
                int issueNumber = -1;
                if (Console.ReadKey().Key == ConsoleKey.Y)
                {
                    Console.Write("Enter Note : ");
                    note = Console.ReadLine();
                    Console.Write("Enter Issue Number: ");
                    issueNumber = int.Parse(Console.ReadLine());
                    Console.Write("Enter ISSN: ");
                    issn = Console.ReadLine();
                }

                var newspaper = new Newspaper(title, pubPlace, pubHouse, pubYear, numOfpages, releaseDate, note, issueNumber, issn);
                if (newspaperLogic.Add(newspaper) == -1)
                {
                    Console.WriteLine($"--------ERROR #2. Wrong input Newspaper data!--------{Environment.NewLine}Infornation was not saved.");
                }
            }
            catch
            {
                Console.WriteLine("You have entered the wrong input data. Try again.");
            }
        }

        public static void AddPatent(ICatalogUnitLogic<Patent> patentLogic, IAuthorLogic authorLogic)
        {
            try
            {
                Console.Write("Enter Title: ");
                string title = Console.ReadLine();
                Console.Write("Enter number of Inventors: ");
                int numOfInventors = int.Parse(Console.ReadLine());

                List<Author> inventors = new List<Author>(0);
                for (int i = 0; i < numOfInventors; i++)
                {
                    string fname, lname = "";
                    Console.Write("Enter first name of inventor: ");
                    fname = Console.ReadLine();

                    Console.WriteLine("If you want to add Last Name to Inventor, press Y.");
                    if (Console.ReadKey().Key == ConsoleKey.Y)
                    {
                        Console.Write("Enter last name of inventor: ");
                        lname = Console.ReadLine();
                    }

                    inventors.Add(new Author(fname, lname));
                }

                Console.Write("Enter country: ");
                string country = Console.ReadLine();
                Console.Write("Enter registration number: ");
                int regNumber = int.Parse(Console.ReadLine());

                Console.WriteLine("If you want to add Application Date, press Y.");
                DateTime? appDate = null;
                if (Console.ReadKey().Key == ConsoleKey.Y)
                {
                    Console.Write("Enter application date(DateTime format): ");
                    appDate = DateTime.Parse(Console.ReadLine());
                }

                Console.Write("Enter publication date: ");
                DateTime pubDate = DateTime.Parse(Console.ReadLine());
                Console.Write("Enter number of pages: ");
                int numOfPages = int.Parse(Console.ReadLine());

                Console.WriteLine("If you want to add Note of Patent, press Y.");
                string note = "";
                if (Console.ReadKey().Key == ConsoleKey.Y)
                {
                    Console.Write("Enter Note: ");
                    note = Console.ReadLine();
                }

                var patent = new Patent(title, country, regNumber, pubDate, numOfPages, appDate, inventors, note);

                IEnumerable<int> authorsIdsList = new List<int>();

                if (patent.Authors.Count != 0)
                {
                    //authorsIdsList = authorLogic.AddAuthors(patent);
                }

                var patentId = patentLogic.Add(patent);

                if (patentId == -1)
                {
                    Console.WriteLine($"--------ERROR #3. Wrong input Book data!--------{Environment.NewLine}Information was not saved.");
                }
                else
                {
                    authorLogic.AddAuthorsToLibraryObject(patentId, authorsIdsList);
                }
            }
            catch
            {
                Console.WriteLine("You have entered the wrong input data. Try again.");
            }
        }

        public static void RemoveUnit(ICatalogLogic catalogLogic)
        {
            Console.Write("Enter ID to remove: ");
            int id = int.Parse(Console.ReadLine());
            catalogLogic.Remove(id);
        }

        public static void SearchByTitle(ICatalogLogic catalogLogic)
        {
            Console.Write("Enter title template to search: ");
            var result = catalogLogic.GetRecordByTitle(Console.ReadLine());
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
        }

        public static void Sort(ICatalogLogic catalogLogic)
        {
            Console.WriteLine();
            Console.WriteLine("_______________________________________________________");
            Console.WriteLine("Enter the sorting criterion (0 - for ASC, 1 - for DESC): ");
            int criterion = int.Parse(Console.ReadLine());
            SortingCriterion sortingCriterion = criterion == 1 ? SortingCriterion.DESCPUBLICATIONYEAR : SortingCriterion.ASCPUBLICATIONYEAR;
            var result = catalogLogic.Sort(sortingCriterion);
            foreach (var item in result)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine("_______________________________________________________");
        }

        public static void SearchBookByAuthor(ICatalogLogic catalogLogic)
        {
            Console.Write("Enter author's first name to search: ");
            string fname = Console.ReadLine();
            Console.Write("Enter author's last name to search: ");
            string lname = Console.ReadLine();
            var result = catalogLogic.FindBookByAuthor(new Author(fname, lname));
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
        }

        public static void SearchPatentByAuthor(ICatalogLogic catalogLogic)
        {
            Console.Write("Enter inventor's first name to search: ");
            string fname = Console.ReadLine();
            Console.Write("Enter inventor's second name to search: ");
            string lname = Console.ReadLine();
            var result = catalogLogic.FindPatentByAuthor(new Author(fname, lname));

            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
        }

        public static void SearchBookAndPatentByAuthor(ICatalogLogic catalogLogic)
        {
            Console.Write("Enter author's first name to search: ");
            string fname = Console.ReadLine();
            Console.Write("Enter author's second name to search: ");
            string lname = Console.ReadLine();
            var result = catalogLogic.FindBooksAndPatentsOfAuthor(new Author(fname, lname));
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
        }

        public static void PubHouseStartsWith(IBookLogic bookLogic)
        {
            Console.Write("Enter text to search by PublishingHouse: ");
            var result = bookLogic.FindAllByPublisherNameTemplateAndGroupByPublisher(Console.ReadLine());
            foreach (var item in result)
            {
                Console.WriteLine($"Publishing House: {item.Key}");
                foreach (var item2 in item.Value)
                {
                    Console.WriteLine($"{item2.Id} | {item2.Title} | {item2.PlaceOfPublication} | {item2.PublishingHouse} | {item2.PublicationOrApplicationYear} | {item2.NumberOfPages} | {item2.Note} | {item2.ISBN}");
                }
            }
        }

        public static void GroupByPubYear(ICatalogLogic catalogLogic)
        {
            var result = catalogLogic.GroupRecordsByYear();

            foreach (var item in result)
            {
                Console.WriteLine($"{item.Key}");
                foreach (var item2 in item.Value)
                {
                    Console.WriteLine($"  {item2.Id} {item2.Title}");
                }
            }
        }

        private static string ReadKey(ref string inp_key)
        {
            inp_key = Console.ReadLine().ToUpper();
            return inp_key;
        }
    }
}