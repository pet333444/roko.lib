﻿using ROKO.Lib.Entities;
using ROKO.Lib.PL.WebApi.Models;
using ROKO.Lib.PL.WebUI.Models;

using System.Collections.Generic;
using System.Linq;

namespace ROKO.Lib.PL.WebApi.Helpers
{
    public static class ConvertHelperscs
    {
        public static IEnumerable<NewCatalogRequirementsViewModel> ToNewCatalogRequrementViewModel(this IEnumerable<LibraryObject> catalog)
        {
            List<NewCatalogRequirementsViewModel> list = new();

            if (catalog is not null
                && catalog.Any())
            {
                foreach (var item in catalog)
                {
                    NewCatalogRequirementsViewModel model = new();

                    if (item is Book book)
                    {
                        model.UnitId = book.Id;

                        model.Name = book.Authors.Count == 0
                                        ? $"{book.Title}({book.PublicationOrApplicationYear})"
                                        : $"{book.AuthorList()} - {book.Title}({book.PublicationOrApplicationYear})";

                        model.Identificator = book.ISBN;
                        model.NumberOfPages = book.NumberOfPages;
                    }else if (item is Newspaper newspaper)
                    {
                        model.UnitId = newspaper.Id;

                        model.Name = newspaper.IssueNumber == -1
                                        ? $"{newspaper.Title} {newspaper.PublicationOrApplicationYear}"
                                        : $"{newspaper.Title} №{newspaper.IssueNumber}/{newspaper.PublicationOrApplicationYear}";

                        model.Identificator = newspaper.ISSN;
                        model.NumberOfPages = newspaper.NumberOfPages;
                    }else if (item is Patent patent)
                    {
                        model.UnitId = patent.Id;
                        model.Name = $"«{patent.Title}» от {patent.PublicationDate:dd.MM.yyyy}";
                        model.Identificator = patent.RegistrationNumber.ToString();
                        model.NumberOfPages = patent.NumberOfPages;
                    }

                    list.Add(model);
                }
            }

            return list;
        }

        public static CatalogIndexViewModelForAPI ToPage(this IEnumerable<LibraryObject> catalog, int pageNumber)
        {
            var pageSize = 20;
            var elementsPerPages = catalog.Skip((pageNumber - 1) * pageSize)
                                          .Take(pageSize);
            PageInfo pageInfo = new(totalItems: catalog.Count(),
                                    page: pageNumber,
                                    pageSize: pageSize);

            return new()
            {
                PageInfo = pageInfo,
                CatalogElements = elementsPerPages
            };
        }

        public static CatalogIndexViewModelForUser ToPage(this IEnumerable<User> catalog, int pageNumber)
        {
            var pageSize = 20;
            var elementsPerPages = catalog.Skip((pageNumber - 1) * pageSize)
                                          .Take(pageSize);
            PageInfo pageInfo = new(totalItems: catalog.Count(),
                                    page: pageNumber,
                                    pageSize: pageSize);

            return new()
            {
                PageInfo = pageInfo,
                CatalogElements = elementsPerPages
            };
        }

        public static CatalogIndexViewModelForAuthor ToPage(this IEnumerable<Author> catalog, int pageNumber)
        {
            var pageSize = 20;
            var elementsPerPages = catalog.Skip((pageNumber - 1) * pageSize)
                                          .Take(pageSize);
            PageInfo pageInfo = new(totalItems: catalog.Count(),
                                    page: pageNumber,
                                    pageSize: pageSize);

            return new()
            {
                PageInfo = pageInfo,
                CatalogElements = elementsPerPages
            };
        }

        public static CatalogIndexViewModelForBooksGroups ToPage(this Dictionary<string, List<Book>> catalog, int pageNumber)
        {
            var pageSize = 20;
            var elementsPerPages = catalog.Skip((pageNumber - 1) * pageSize)
                                          .Take(pageSize);
            PageInfo pageInfo = new(totalItems: catalog.Count,
                                    page: pageNumber,
                                    pageSize: pageSize);

            return new()
            {
                PageInfo = pageInfo,
                CatalogElements = elementsPerPages
            };
        }
    }
}