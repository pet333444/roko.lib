﻿using ROKO.Lib.BLL.Interface;
using ROKO.Lib.Entities;

using System.Collections.Generic;
using System.Linq;

namespace ROKO.Lib.PL.WebApi.Helpers
{
    public static class LibraryElementHelper
    {
        public static void SelectAllAuthorsForLibraryElements(this IEnumerable<LibraryObject> catalog,
                                                              IAuthorLogic authorLogic)
        {
            if (catalog is not null
                && catalog.Any())
            {
                foreach (var item in catalog)
                {
                    if (item is Book book)
                    {
                        var authors = authorLogic.GetAuthorsByLibraryElementID(item.Id);
                        book.Authors = new(authors);
                    }
                    else if (item is Patent patent)
                    {
                        var authors = authorLogic.GetAuthorsByLibraryElementID(item.Id);
                        patent.Authors = new(authors);
                    }
                }
            }
        }
    }
}