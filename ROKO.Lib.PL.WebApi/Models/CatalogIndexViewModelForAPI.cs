﻿using ROKO.Lib.Entities;
using ROKO.Lib.PL.WebUI.Models;

using System.Collections.Generic;

namespace ROKO.Lib.PL.WebApi.Models
{
    public class CatalogIndexViewModelForAPI
    {
        public IEnumerable<LibraryObject> CatalogElements { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}