﻿using ROKO.Lib.Entities;
using ROKO.Lib.PL.WebUI.Models;

using System.Collections.Generic;

namespace ROKO.Lib.PL.WebApi.Models
{
    public class CatalogIndexViewModelForBooksGroups
    {
        public IEnumerable<KeyValuePair<string, List<Book>>> CatalogElements { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}