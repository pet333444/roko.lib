﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.Entities;
using ROKO.Lib.PL.WebApi.Helpers;

using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ROKO.Lib.PL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogController : ControllerBase
    {
        private IBookLogic BookLogic { set; get; }
        private IAuthorLogic AuthorLogic { set; get; }
        private ICatalogLogic CatalogLogic { set; get; }
        private ILogger<CatalogController> Logger { set; get; }

        public CatalogController(IBookLogic bookLogic,
                                ICatalogLogic catalogLogic,
                                IAuthorLogic authorLogic,
                                ILogger<CatalogController> logger)
        {
            BookLogic = bookLogic;
            AuthorLogic = authorLogic;
            CatalogLogic = catalogLogic;
            Logger = logger;
        }

        [HttpPost(nameof(PatentByAuthor))]
        public IActionResult PatentByAuthor( Author author, int? pageNumber)
        {
            IActionResult result;
            try
            {
                pageNumber ??= 1;

                var elements = CatalogLogic.FindPatentByAuthor(author);
                elements.SelectAllAuthorsForLibraryElements(AuthorLogic);

                var page = elements.ToPage(pageNumber.Value);
                result = Ok(page);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [HttpPost(nameof(BookByAuthor))]
        public IActionResult BookByAuthor( Author author, int? pageNumber)
        {
            IActionResult result;
            try
            {
                pageNumber ??= 1;

                var elements = CatalogLogic.FindBookByAuthor(author);
                elements.SelectAllAuthorsForLibraryElements(AuthorLogic);

                var page = elements.ToPage(pageNumber.Value);
                result = Ok(page);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [HttpPost(nameof(PatentAndBookByAuthor))]
        public IActionResult PatentAndBookByAuthor( Author author, int? pageNumber)
        {
            IActionResult result;
            try
            {
                pageNumber ??= 1;

                var elements = CatalogLogic.FindBooksAndPatentsOfAuthor(author);

                elements.SelectAllAuthorsForLibraryElements(AuthorLogic);                var page = elements.ToPage(pageNumber.Value);

                result = Ok(page);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [HttpGet(nameof(AllElements))]
        public IActionResult AllElements(int? pageNumber)
        {
            IActionResult result;
            try
            {
                pageNumber ??= 1;

                var elements = CatalogLogic.GetCatalog();

                elements.SelectAllAuthorsForLibraryElements(AuthorLogic);                var page = elements.ToPage(pageNumber.Value);

                result = Ok(page);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [HttpGet(nameof(AllElementsByTitle) + "/{titlePart}")]
        public IActionResult AllElementsByTitle(string titlePart, int? pageNumber)
        {
            IActionResult result;
            try
            {
                pageNumber ??= 1;

                var elements = CatalogLogic.GetRecordByTitle(titlePart);

                elements.SelectAllAuthorsForLibraryElements(AuthorLogic);                var page = elements.ToPage(pageNumber.Value);

                result = Ok(page);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [HttpGet(nameof(SortByYearOfPublication) + "/{sortingType}")]
        public IActionResult SortByYearOfPublication(SortingCriterion sortingType, int? pageNumber)
        {
            IActionResult result;
            try
            {
                pageNumber ??= 1;

                var elements = CatalogLogic.Sort(sortingType);

                elements.SelectAllAuthorsForLibraryElements(AuthorLogic);                var page = elements.ToPage(pageNumber.Value);

                result = Ok(page);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [HttpGet(nameof(AllBooksWherePublisherstartWithPartAndGroupByPublisher) + "/{publisherNameStartPart}")]
        public IActionResult AllBooksWherePublisherstartWithPartAndGroupByPublisher(string publisherNameStartPart, int? pageNumber)
        {
            IActionResult result;
            try
            {
                pageNumber ??= 1;

                var elements = BookLogic.FindAllByPublisherNameTemplateAndGroupByPublisher(publisherNameStartPart);
                var page = elements.ToPage(pageNumber.Value);

                result = Ok(page);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }
    }
}