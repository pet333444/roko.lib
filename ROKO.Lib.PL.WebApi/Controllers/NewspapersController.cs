﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.Entities;
using ROKO.Lib.PL.WebApi.Helpers;

using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ROKO.Lib.PL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewspapersController : ControllerBase
    {
        private ICatalogLogic CatalogLogic { set; get; }
        private ICatalogUnitLogic<Newspaper> CalalogUnitLogic { set; get; }
        private ILogger<NewspapersController> Logger { set; get; }

        public NewspapersController(ICatalogLogic catalogLogic,
                                    ICatalogUnitLogic<Newspaper> calalogUnitLogic,
                                    ILogger<NewspapersController> logger)
        {
            CatalogLogic = catalogLogic;
            CalalogUnitLogic = calalogUnitLogic;
            Logger = logger;
        }

        [HttpGet]
        public IActionResult Get(int? pageNumber)
        {
            IActionResult result;
            try
            {
                var page = CalalogUnitLogic
                                    .GetAllSpecified()
                                     ?.ToPage(pageNumber.Value);
                result = Ok(page);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            IActionResult result;
            try
            {
                var elem = CatalogLogic.FindCatalogElementById(id) as Newspaper;

                result = Ok(elem);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Post([FromBody] Newspaper newspaper)
        {
            IActionResult result;

            try
            {
                result = ModelState.IsValid
                            && CalalogUnitLogic.Add(newspaper) > -1
                         ? Ok()
                         : ValidationProblem();
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [Authorize(Roles = "Administrator")]
        [HttpPut("{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Put(int id, [FromBody] Newspaper newspaper)
        {
            IActionResult result;
            try
            {
                if (ModelState.IsValid)
                {
                    newspaper.Id = id;
                    CalalogUnitLogic.Edit(newspaper);
                    result = Ok();
                }
                else
                {
                    result = ValidationProblem();
                }
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete("{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            IActionResult result;
            try
            {
                result = CatalogLogic.Remove(id)
                         ? Ok()
                         : BadRequest();
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");

                result = BadRequest();
            }
            return result;
        }
    }
}