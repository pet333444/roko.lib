﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.Entities;
using ROKO.Lib.PL.WebUI.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ROKO.Lib.PL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private IUserLogic UserLogic { set; get; }
        private ILogger<AccountController> Logger { set; get; }

        public AccountController(IUserLogic userLogic,
                                ILogger<AccountController> logger)
        {
            UserLogic = userLogic;
            Logger = logger;
        }

        [HttpPost(nameof(LogIn))]
        [ValidateAntiForgeryToken]
        public IActionResult LogIn([FromBody] LoginViewModel userCredentials)
        {
            IActionResult result;
            try
            {
                string hash = "";

                using (SHA512 sha512Hash = SHA512.Create())
                {
                    byte[] sourceBytes = Encoding.Unicode.GetBytes(userCredentials.Password);
                    byte[] hashBytes = sha512Hash.ComputeHash(sourceBytes);
                    hash = BitConverter.ToString(hashBytes).Replace("-", string.Empty);
                }

                if (ModelState.IsValid)
                {
                    User user = UserLogic.GetUserByLogin(userCredentials.Login);

                    if (user != null && user.Password == hash)
                    {
                        var claims = new List<Claim>
                    {
                        new (ClaimTypes.Name, user.Login),
                        new (ClaimTypes.Role, user.Role)
                    };

                        var claimsIdentity = new ClaimsIdentity(claims: claims,
                                                                authenticationType: CookieAuthenticationDefaults.AuthenticationScheme);

                        Logger.LogInformation($"Message: User '{user.Login}' authenticated. " +
                                                $"Time: {DateTime.UtcNow} " +
                                                $"Layer: {nameof(PL)} " +
                                                $"Class: {nameof(WebUI.Controllers.AccountController)} " +
                                                $"Method: {nameof(AccountController.LogIn)}");

                        result = SignIn(new(claimsIdentity));
                    }
                    else
                    {
                        result = ValidationProblem();
                    }
                }
                else
                {
                    result = ValidationProblem();
                }
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [HttpPost(nameof(Registration))]
        [ValidateAntiForgeryToken]
        public IActionResult Registration([FromBody] RegisterViewModel registerViewModel)
        {
            IActionResult result;

            try
            {
                if (ModelState.IsValid)
                {
                    var user = UserLogic
                                    .GetAll()
                                    .FirstOrDefault(x => x.Login == registerViewModel.Login);

                    if (user == null)
                    {
                        using (var sha512Hash = SHA512.Create())
                        {
                            var sourceBytes = Encoding.Unicode.GetBytes(registerViewModel.Password);
                            var hashBytes = sha512Hash.ComputeHash(sourceBytes);
                            var hash = BitConverter.ToString(hashBytes).Replace("-", string.Empty);

                            user = new()
                            {
                                Login = registerViewModel.Login,
                                Password = hash,
                                Role = "User"
                            };
                        }

                        UserLogic.AddUser(user);

                        Logger.LogInformation($"Message: Registration with login '{registerViewModel.Login}' successful. " +
                                                $"Time: {DateTime.UtcNow} " +
                                                $"Layer: {nameof(PL)} " +
                                                $"Class: {nameof(AccountController)} " +
                                                $"Method: {nameof(Registration)}");

                        result = Ok();
                    }
                    else
                    {
                        result = ValidationProblem();
                    }
                }
                else
                {
                    result = ValidationProblem();
                }
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [HttpPost(nameof(SignOut))]
        [ValidateAntiForgeryToken]
        public new IActionResult SignOut()
        {
            IActionResult result;
            try
            {
                result = base.SignOut();
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }
    }
}