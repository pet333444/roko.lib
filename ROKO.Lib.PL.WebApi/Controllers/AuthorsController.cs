﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.PL.WebApi.Helpers;

using System;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ROKO.Lib.PL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private IAuthorLogic AuthorLogic { set; get; }
        private ILogger<AuthorsController> Logger { set; get; }

        public AuthorsController(IAuthorLogic authorLogic,
                                 ILogger<AuthorsController> logger)
        {
            AuthorLogic = authorLogic;
            Logger = logger;
        }

        [HttpGet]
        public IActionResult Get(int? pageNumber)
        {
            IActionResult result;
            try
            {
                pageNumber ??= 1;
                var elems = AuthorLogic
                            .GetAuthors()
                            .ToPage(pageNumber.Value);

                result = Ok(elems);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            IActionResult result;
            try
            {
                var elem = AuthorLogic.GetAuthors()
                                       .Where(elem => elem.AuthorID == id);

                result = Ok(elem);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }
    }
}