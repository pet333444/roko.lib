﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.Entities;
using ROKO.Lib.PL.WebApi.Helpers;

using System;
using System.Data;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ROKO.Lib.PL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private IBookLogic BookLogic { set; get; }
        private IAuthorLogic AuthorLogic { set; get; }
        private ICatalogLogic CatalogLogic { set; get; }
        private ILogger<BooksController> Logger { set; get; }

        public BooksController(IBookLogic bookLogic,
                               ICatalogLogic catalogLogic,
                               IAuthorLogic authorLogic,
                               ILogger<BooksController> logger)
        {
            BookLogic = bookLogic;
            AuthorLogic = authorLogic;
            CatalogLogic = catalogLogic;
            Logger = logger;
        }

        [HttpGet]
        public IActionResult Get(int? pageNumber)
        {
            IActionResult result;
            try
            {
                pageNumber ??= 1;
                var books = BookLogic
                            .GetAllSpecified();

                books.SelectAllAuthorsForLibraryElements(AuthorLogic);

                var page = books
                            ?.ToPage(pageNumber.Value);

                result = Ok(page);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            IActionResult result;
            try
            {
                var elem = CatalogLogic.FindCatalogElementById(id) as Book;

                result = Ok(elem);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public IActionResult Post([FromBody] Book book)
        {
            IActionResult result;

            try
            {
                if (ModelState.IsValid)
                {
                    var id = BookLogic.Add(book);

                    if (id > -1)
                    {
                        var authorsId = book
                                        .Authors
                                        .Select(elem => elem.AuthorID);

                        AuthorLogic.AddAuthorsToLibraryObject(id, authorsId);

                        result = Ok();
                    }
                    else
                    {
                        result = BadRequest();
                    }
                }
                else
                {
                    result = ValidationProblem();
                }
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [Authorize(Roles = "Administrator")]
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Book book)
        {
            IActionResult result;
            try
            {
                if (ModelState.IsValid)
                {
                    book.Id = id;
                    BookLogic.Edit(book);
                    result = Ok();
                }
                else
                {
                    result = ValidationProblem();
                }
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            IActionResult result;
            try
            {
                result = CatalogLogic.Remove(id)
                         ? Ok()
                         : BadRequest();
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");

                result = BadRequest();
            }
            return result;
        }
    }
}