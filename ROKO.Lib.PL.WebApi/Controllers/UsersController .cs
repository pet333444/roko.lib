﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.PL.WebApi.Helpers;

using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ROKO.Lib.PL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Administrator")]
    public class UsersController : ControllerBase
    {
        private IUserLogic UserLogic { set; get; }
        private ILogger<UsersController> Logger { set; get; }

        public UsersController(IUserLogic userLogic,
                                ILogger<UsersController> logger)
        {
            UserLogic = userLogic;
            Logger = logger;
        }

        [HttpGet("{pageNumber?}")]
        public IActionResult Get(int? pageNumber)
        {
            IActionResult result;
            try
            {
                var elemets = UserLogic.GetAll();
                var page = elemets.ToPage(pageNumber.Value);

                result = Ok(page);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [HttpGet("{login}")]
        public IActionResult Get(string login)
        {
            IActionResult result;
            try
            {
                var elemets = UserLogic.GetUserByLogin(login);

                result = Ok(elemets);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [HttpPut(nameof(ChangeRole) + "/{id}/{role}")]
        [ValidateAntiForgeryToken]
        public IActionResult ChangeRole(int id, string role)
        {
            IActionResult result;
            try
            {
                UserLogic.UpdateRole(new()
                {
                    Id = id,
                    Role = role
                });
                Logger.LogInformation($"Message: Admin with login '{User.Identity.Name}' updated role of user " +
                                        $"with id = {id}. Time: {DateTime.UtcNow} Layer: {nameof(PL)} " +
                                        $"Class: {nameof(UsersController)}" +
                                        $" Method: {nameof(ChangeRole)}");
                return RedirectToAction("Index", "Admin");
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }
    }
}