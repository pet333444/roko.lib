﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using ROKO.Lib.BLL.Interface;
using ROKO.Lib.Entities;
using ROKO.Lib.PL.WebApi.Helpers;

using System;
using System.Data;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ROKO.Lib.PL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatentsController : ControllerBase
    {
        private IAuthorLogic AuthorLogic { set; get; }
        private ICatalogLogic CatalogLogic { set; get; }
        private ICatalogUnitLogic<Patent> CalalogUnitLogic { set; get; }
        private ILogger<PatentsController> Logger { set; get; }

        public PatentsController(ICatalogLogic catalogLogic,
                                 ICatalogUnitLogic<Patent> calalogUnitLogic,
                                 IAuthorLogic authorLogic,
                                 ILogger<PatentsController> logger)
        {
            AuthorLogic = authorLogic;
            CatalogLogic = catalogLogic;
            CalalogUnitLogic = calalogUnitLogic;
            Logger = logger;
        }

        [HttpGet]
        public IActionResult Get(int? pageNumber)
        {
            IActionResult result;
            try
            {
                pageNumber ??= 1;
                var patents = CalalogUnitLogic.GetAllSpecified();

                patents.SelectAllAuthorsForLibraryElements(AuthorLogic);

                var page = patents?.ToPage(pageNumber.Value);

                result = Ok(page);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            IActionResult result;
            try
            {
                var elem = CatalogLogic.FindCatalogElementById(id) as Patent;

                result = Ok(elem);
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Post([FromBody] Patent patent)
        {
            IActionResult result;

            try
            {
                if (ModelState.IsValid)
                {
                    var id = CalalogUnitLogic.Add(patent);

                    if (id > -1)
                    {
                        var authorsId = patent
                                        .Authors
                                        .Select(elem => elem.AuthorID);

                        AuthorLogic.AddAuthorsToLibraryObject(id, authorsId);

                        result = Ok();
                    }
                    else
                    {
                        result = BadRequest();
                    }
                }
                else
                {
                    result = ValidationProblem();
                }
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [Authorize(Roles = "Administrator")]
        [HttpPut("{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Put(int id, [FromBody] Patent patent)
        {
            IActionResult result;
            try
            {
                if (ModelState.IsValid)
                {
                    patent.Id = id;
                    CalalogUnitLogic.Edit(patent);
                    result = Ok();
                }
                else
                {
                    result = ValidationProblem();
                }
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");
                result = BadRequest();
            }
            return result;
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete("{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id)
        {
            IActionResult result;
            try
            {
                result = CatalogLogic.Remove(id)
                         ? Ok()
                         : BadRequest();
            }
            catch (Exception e)
            {
                Logger.LogError($"ErrorMessage: {e.Message} "
                                 + $"Time: {DateTime.UtcNow} "
                                 + $"Layer: {nameof(PL)} "
                                 + $"Class: {e.TargetSite.DeclaringType.Name} "
                                 + $"Method: {nameof(e.TargetSite.Name)}");

                result = BadRequest();
            }
            return result;
        }
    }
}