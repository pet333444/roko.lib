
CREATE DATABASE [ROKO.Lib.Auth]
 CONTAINMENT = NONE
Go

USE [ROKO.Lib.Auth]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 21-May-21 13:53:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Login] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[Role] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[AddUser]    Script Date: 21-May-21 13:53:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[AddUser]
@Id int out,
@Login nvarchar(50),
@Password nvarchar(128),
@Role nvarchar(50)
AS
Begin
	Insert into Users(Login, Password, Role)
	Values (@Login, @Password, @Role)
	SET @Id = SCOPE_IDENTITY()
end
GO
/****** Object:  StoredProcedure [dbo].[EditUserRole]    Script Date: 21-May-21 13:53:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[EditUserRole]
@Id int,
@Role nvarchar(50)
AS
Begin
	Update Users
	Set [Role] = @Role
	where [Id] = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[GetAll]    Script Date: 21-May-21 13:53:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[GetAll]
AS
Begin
	select Id, Login, Password, Role from Users
end
GO
USE [master]
GO
ALTER DATABASE [ROKO.Lib.Auth] SET  READ_WRITE 
GO


CREATE proc [dbo].[GetUserByLogin]
@Login nvarchar(50)
AS
Begin
    select * from [Users]
    where Login = @Login
end
GO