﻿using ROKO.Lib.Entities;

namespace ROKO.Lib.BLL.Interface
{
    public interface IUniqueChecker<T> where T : LibraryObject
    {
        bool Check(T input);
    }
}