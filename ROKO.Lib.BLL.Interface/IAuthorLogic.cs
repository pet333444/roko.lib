﻿using ROKO.Lib.Entities;

using System.Collections.Generic;

namespace ROKO.Lib.BLL.Interface
{
    public interface IAuthorLogic
    {
        IEnumerable<Author> GetAuthors();

        CatalogUnitWithAuthors AddAuthors(CatalogUnitWithAuthors unit);

        void AddAuthorsToLibraryObject(int libraryObjectId, IEnumerable<int> authorsIds);

        public IEnumerable<Author> GetAuthorsByLibraryElementID(int ID);
    }
}