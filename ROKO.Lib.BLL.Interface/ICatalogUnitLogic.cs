﻿using ROKO.Lib.Entities;

using System.Collections.Generic;

namespace ROKO.Lib.BLL.Interface
{
    public interface ICatalogUnitLogic<T> where T : LibraryObject
    {
        int Add(T unit);

        void Edit(T unit);

        IEnumerable<T> GetAllSpecified();
    }
}